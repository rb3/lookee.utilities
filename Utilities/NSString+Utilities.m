//
//  NSString+Utilities.m
//  Writeability
//
//  Created by Ryan on 8/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "NSString+Utilities.h"

#import "LMacros.h"

#import "NSObject+Utilities.h"

#import "NSData+Utilities.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSString+Convert
#pragma mark -
//*********************************************************************************************************************//




@implementation NSString (Convert)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSUInteger)convertToBase:(NSUInteger)base
{
    const char  *string = [self.uppercaseString cStringUsingEncoding:NSASCIIStringEncoding];
    NSUInteger  length  = [self length];

    NSUInteger  power   = (length -1);

    NSUInteger index    = 0;
    NSUInteger number   = 0;
    NSUInteger value    = 0;

    for (; index < length; index ++) {
        if (string[index] >= 'A' && string[index] <= 'Z') {
            value = string[index] -65+10;
        } else {
            value = string[index] -48;
        }

        number += value * pow(base, power);
        power --;
    }

    return number;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSString+Representation
#pragma mark -
//*********************************************************************************************************************//




@implementation NSString (Representation)

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const kNSStringRepresentationEncyptionKey
=
@"LOOKEE-z19s!v#i!&hq)_$kj$t^@8(ypf)=bdh!(t%(*q6=6x*39@+jl2";


#pragma mark - Public Methods
//*********************************************************************************************************************//

+ (instancetype)stringFromReadableRepresentation:(NSString *)representation
{
    return [[NSString alloc] initWithData:[NSData dataFromBase58Representation:representation] encoding:NSUTF8StringEncoding];
}

+ (instancetype)stringFromCompactRepresentation:(NSString *)representation
{
    return [[NSString alloc] initWithData:[NSData dataFromBase64Representation:representation] encoding:NSUTF8StringEncoding];
}

+ (instancetype)stringFromObjectRepresentation:(id)representation
{
    NSString *JSONString;

    if ([NSJSONSerialization isValidJSONObject:representation]) {
        NSError *error  = nil;
        NSData  *JSON   = [NSJSONSerialization dataWithJSONObject:representation options:0 error:&error];

        if (!error) {
            JSONString = [[NSString alloc] initWithData:JSON encoding:NSUTF8StringEncoding];
        } else {
            DBGError(@"%@", error.description);
        }
    } else {
        DBGError(@"Invalid JSON object '%@'", representation);
    }

    return JSONString;
}

- (NSString *)readableRepresentation
{
    return [[self dataUsingEncoding:NSUTF8StringEncoding] base58Representation];
}

- (NSString *)compactRepresentation
{
    return [[self dataUsingEncoding:NSUTF8StringEncoding] base64Representation];
}

- (id)objectRepresentation
{
    id object;

    NSError *error  = nil;
    NSData  *JSON   = [self dataUsingEncoding:NSUTF8StringEncoding];

    object = [NSJSONSerialization JSONObjectWithData:JSON options:NSJSONReadingAllowFragments error:&error];

    if (error) {
        DBGError(@"%@", error.description);
    }
    
    return object;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSString+Substring
#pragma mark -
//*********************************************************************************************************************//




@implementation NSString (Substring)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)hasSubstring:(NSString *)substring
{
    return ([self rangeOfString:substring].location != NSNotFound);
}

- (NSString *)stringByDeletingSubstring:(NSString *)substring
{
    return [self stringByReplacingOccurrencesOfString:substring withString:@""];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSString+Pattern
#pragma mark -
//*********************************************************************************************************************//




@implementation NSString (Match)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)isMatch:(NSString *)pattern
{
    BOOL result = NO;

    NSError *error;

    id scanner
    =
    [NSRegularExpression
     regularExpressionWithPattern   :pattern
     options                        :NSRegularExpressionCaseInsensitive
     error                          :&error];

    if (!error) {
        NSRange range = ((NSRange){0, self.length});

        id match = [scanner firstMatchInString:self options:0 range:range];

        if (match) {
            result = NSEqualRanges([match range], range);
        }
    } else {
        DBGError(@"%@", error.description);
    }

    return result;
}

- (NSArray *)match:(NSString *)pattern
{
    NSMutableArray *results = [NSMutableArray new];

    NSError *error;

    id scanner
    =
    [NSRegularExpression
     regularExpressionWithPattern   :pattern
     options                        :NSRegularExpressionCaseInsensitive
     error                          :&error];

    if (!error) {
        NSArray *matches = [scanner
                            matchesInString :self
                            options         :0
                            range           :((NSRange) {
            .location   = 0,
            .length     = self.length
        })];

        for (NSTextCheckingResult *match in matches) {
            NSUInteger rangeCount = [match numberOfRanges];

            if (rangeCount > 1) {
                for (NSUInteger index = 1; index < rangeCount; index ++) {
                    [results addObject:[self substringWithRange:[match rangeAtIndex:index]]];
                }
            } else {
                [results addObject:[self substringWithRange:match.range]];
            }
        }
    } else {
        DBGError(@"%@", error.description);
    }

    return [results copy];
}

@end
