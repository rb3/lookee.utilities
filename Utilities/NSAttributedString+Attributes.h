//
//  NSAttributedString+Attributes.h
//  Writeability
//
//  Created by Ryan on 1/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface NSAttributedString (Attributes)

- (UIColor *)colorAtIndex:(NSUInteger)index;
- (UIFont *)fontAtIndex:(NSUInteger)index;

- (UIColor *)lastColor;
- (UIFont *)lastFont;

@end
