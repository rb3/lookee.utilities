//
//  NSThread+Block.h
//  Writeability
//
//  Created by Ryan on 10/27/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSThread (Block)

+ (void)performBlockOnMainThread:(void(^)(void))block;
+ (void)performBlockInBackground:(void(^)(void))block;


- (void)performBlock:(void(^)(void))block;
- (void)performBlock:(void(^)(void))block waitUntilDone:(BOOL)wait;
- (void)performBlock:(void(^)(void))block afterDelay:(NSTimeInterval)delay;

@end
