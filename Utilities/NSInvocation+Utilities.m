//
//  NSInvocation+Utilities.m
//  Utilities
//
//  Created by Ryan Blonna on 16/1/15.
//  Copyright (c) 2015 Lookee. All rights reserved.
//


#import "NSInvocation+Utilities.h"





//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSInvocation+Boxed
#pragma mark -
//*********************************************************************************************************************//





@implementation NSInvocation (Boxed)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)invocationFromBoxedRepresentation:(id)representation
{
    return [[self alloc] initWithBoxedRepresentation:representation];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithBoxedRepresentation:(id)representation
{
    id encoding   = representation[0];

    id signature  = [NSMethodSignature signatureWithObjCTypes:[encoding UTF8String]];

    if ((self = [NSInvocation invocationWithMethodSignature:signature])) {
        SEL selector = NSSelectorFromString(representation[1]);

        [self setSelector:selector];

        for (NSUInteger
             count = self.methodSignature.numberOfArguments,
             index = 2;
             index < count;
             index ++)
        {
            const char  *encoding   = [self.methodSignature getArgumentTypeAtIndex:index];

            id          argument    = representation[index];

            if (encoding[0] == '@') {
                if (argument != [NSNull null]) {
                    [self setArgument:&argument atIndex:index];
                }
            } else {
                NSUInteger bufferSize; NSGetSizeAndAlignment(encoding, &bufferSize, NULL);

                void *buffer = malloc(bufferSize);
                {
                    [argument getValue:buffer];
                    [self setArgument:buffer atIndex:index];
                }
                free(buffer);
            }
        }
    }

    return self;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (id)boxedRepresentation
{
    NSMutableArray *representation = [NSMutableArray new];

    [representation addObject:[self.methodSignature typeEncoding]];
    [representation addObject:NSStringFromSelector([self selector])];

    for (NSUInteger
         count = self.methodSignature.numberOfArguments,
         index = 2;
         index < count;
         index ++)
    {
        const char *encoding = [self.methodSignature getArgumentTypeAtIndex:index];

        if (encoding[0] == '@') {
            __unsafe_unretained id argument;

            [self getArgument:&argument atIndex:index];
            [representation addObject:argument?:[NSNull null]];
        } else {
            NSUInteger bufferSize; NSGetSizeAndAlignment(encoding, &bufferSize, NULL);

            void *buffer = malloc(bufferSize);
            {
                [self getArgument:buffer atIndex:index];
                [representation addObject:[NSValue value:buffer withObjCType:encoding]];
            }
            free(buffer);
        }
    }

    return [representation copy];
}

- (id)boxedReturnValue
{
    const char *encoding = [self.methodSignature methodReturnType];

    if (strcmp(encoding, @encode(void)) == 0) {
        return nil;
    }

    id returnValue;

    if (encoding[0] == '@') {
        id __unsafe_unretained object;

        [self getReturnValue:&object], (returnValue = object);
    } else {
        NSUInteger bufferSize; NSGetSizeAndAlignment(encoding, &bufferSize, NULL);

        void *buffer = malloc(bufferSize);
        {
            [self getReturnValue:buffer];
            returnValue = [NSValue value:buffer withObjCType:encoding];
        }
        free(buffer);
    }

    return (returnValue?:[NSNull null]);
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSMethodSignature+TypeEncoding
#pragma mark -
//*********************************************************************************************************************//





@implementation NSMethodSignature (TypeEncoding)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSString *)typeEncoding
{
    size_t encodingCount    = [self numberOfArguments];
    size_t encodingLength   = strlen([self methodReturnType]);

    for (NSUInteger
         index = 0;
         index < encodingCount;
         index ++)
    {
        encodingLength += strlen([self getArgumentTypeAtIndex:index]);
    }

    encodingLength ++;

    char *encoding = calloc(encodingLength, 1);

    strlcpy(encoding, [self methodReturnType], encodingLength);

    for (NSUInteger
         index = 0;
         index < encodingCount;
         index ++)
    {
        size_t currentLength = strlen(encoding);

        strlcpy(encoding + currentLength, [self getArgumentTypeAtIndex:index], encodingLength - currentLength);
    }

    id typeEncoding = [NSString stringWithUTF8String:encoding];

    free(encoding);
    
    return typeEncoding;
}

@end
