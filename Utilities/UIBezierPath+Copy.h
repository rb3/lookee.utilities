//
//  UIBezierPath+Copy.h
//  Writeability
//
//  Created by Ryan on 11/14/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>



@interface UIBezierPath (Copy)

+ (UIBezierPath *)bezierPathCopyByStrokingRect:(CGRect)rect lineWidth:(CGFloat)lineWidth;

@end
