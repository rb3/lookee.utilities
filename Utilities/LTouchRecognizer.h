//
//  LGestureRecognizer.h
//  Writeability
//
//  Created by Jelo Agnasin on 10/8/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LTouchRecognizer : UIGestureRecognizer

@property (nonatomic, readonly, assign) CGPoint location;
@property (nonatomic, readonly, assign) CGPoint startPoint;
@property (nonatomic, readonly, assign) CGPoint previousPoint;

@property (nonatomic, readonly, assign) BOOL    didBegin;
@property (nonatomic, readonly, assign) BOOL    justBegan;
@property (nonatomic, readonly, assign) BOOL    didMove;

@end
