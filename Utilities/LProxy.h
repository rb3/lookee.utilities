//
//  LProxy.h
//  Writeability
//
//  Created by Ryan on 8/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LProxy : NSProxy
{
@protected
    id object;
    id objectLock;
}

+ (id)proxy;
+ (id)proxyWithObject:(id)object;


- (id)$direct;

@end


@interface NSObject (LProxy)

- (instancetype)$direct;

@end
