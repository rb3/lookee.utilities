//
//  NSObject+Utilities.h
//  Writeability
//
//  Created by Ryan Blonna on 14/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>

#import "LKVO.h"



typedef id(^LKVOVerb)(void(^)(/* LKVONotification * */));
typedef id(^LMapVerb)(id);
typedef id(^LNotificationVerb)(void(^)(/* NSNotification * */));
typedef id(^LFileMonitorVerb)(void(^)(/* unsigned long */));
typedef id(^LDeallocVerb)(void(^)(/* __unsafe_unretained id */));



@interface NSObject (Invocation)

- (NSInvocation *)invocationForSelector:(SEL)selector;
- (NSInvocation *)retainingInvocationForSelector:(SEL)selector;

@end


@interface NSObject (Observer)

- (LKVOVerb)observerForKeyPath:(NSString *)keyPath;
- (LKVOVerb)observerForKeyPaths:(NSArray *)keyPaths;

- (LKVOVerb)observerForKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options;
- (LKVOVerb)observerForKeyPaths:(NSArray *)keyPaths options:(NSKeyValueObservingOptions)options;

- (LKVOVerb)initializedObserverForKeyPath:(NSString *)keyPath;
- (LKVOVerb)initializedObserverForKeyPaths:(NSArray *)keyPaths;

- (LMapVerb)mapKeyPath:(NSString *)keyPath;
- (LMapVerb)mapKeyPaths:(NSArray *)keyPaths;

- (void)entangleKeyPath:(NSString *)source withKeyPath:(NSString *)destination;
- (void)entangleKeyPaths:(NSArray *)sources withKeyPaths:(NSArray *)destinations;

- (LNotificationVerb)observerForNotificationKey:(NSString *)notificationKey;

- (LFileMonitorVerb)observerForFileAtPath:(NSString *)filePath;

- (LDeallocVerb)observerForDealloc;

- (void)destroyKeyPathObserver:(id)observer;
- (void)destroyObserversForKeyPath:(NSString *)keyPath;
- (void)destroyObserversForKeyPaths:(NSArray *)keyPaths;

- (void)destroyNotificationObserver:(id)observer;

- (void)destroyFileObserver:(id)observer;

- (void)destroyDeallocObserver:(id)observer;

@end


@interface NSObject (Runtime)

@property (nonatomic, readwrite, strong) id associatedObject;


+ (NSOrderedSet *)superclasses;

+ (NSSet *)attributes;
+ (NSSet *)allAttributes;

+ (NSDictionary *)infoForAttribute:(NSString *)attribute;
+ (NSString *)encodingForAttribute:(NSString *)attribute;
+ (BOOL)hasSetterForAttribute:(NSString *)attribute;
+ (BOOL)hasMutableAttribute:(NSString *)attribute;

+ (NSString *)encodingForMethodSelector:(SEL)selector;

+ (void)forEachMethodSelector:(void(^)(SEL selector, BOOL *stop))block;


+ (Class)declareSubclassNamed:(NSString *)name;

+ (void)addPropertyWithName:(NSString *)name typeEncoding:(NSString *)typeEncoding;

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy;

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy
isAtomic            :(BOOL      )isAtomic;

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy
isAtomic            :(BOOL      )isAtomic
getterName          :(NSString *)getterName;

+ (void)
addPropertyWithName :(NSString *)name
typeEncoding        :(NSString *)typeEncoding
shouldCopy          :(BOOL      )shouldCopy
isAtomic            :(BOOL      )isAtomic
getterName          :(NSString *)getterName
setterName          :(NSString *)setterName;

+ (void)addMethodSelector:(SEL)selector withImplementation:(void *)implementation andEncoding:(NSString *)encoding;
+ (void)addMethodWithSelector:(SEL)selector withBlock:(id)block;
+ (void)addMethodSelector:(SEL)selector fromType:(Class)type;
+ (void)overrideDeallocWithBlock:(void(^)(__unsafe_unretained id object, void(^dealloc)(void)))block;

- (id)associatedValueForKey:(const void *)key;
- (void)setAssociatedValue:(id)value forKey:(const void *)key;

- (NSDictionary *)propertyDump;

@end

@interface LMethodDescriptor : NSObject
{
@public
    SEL selector;
    id  block;
}

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wfunction-def-in-objc-container"

static inline LMethodDescriptor *LMethodDescriptorNew(SEL selector, id block) {
    LMethodDescriptor *descriptor = [LMethodDescriptor new];
    descriptor->selector    = selector;
    descriptor->block       = block;

    return descriptor;
}

#pragma clang diagnostic pop

@end


/* !!!: This category allows serialization of objects that are subclasses 
 * of NSNull, NSNumber, NSString, NSArray, NSDictionary, and NSOrderedSet.
 * Custom objects can be tagged as serializable by conforming to the 
 * LSerializable protocol.
 */
@interface NSObject (Serialization)

id serialize(id object, id(^verb)(id object));
id deserialize(id serialized, id(^verb)(Class type, id parameter));


+ (id)newFromJSONString:(NSString *)JSONString;
+ (id)newFromJSONString:(NSString *)JSONString deserializer:(id(^)(Class, id))verb;

- (NSString *)convertToJSONString;
- (NSString *)convertToJSONString:(id(^)(id))verb;

@end
