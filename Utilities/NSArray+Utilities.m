//
//  NSArray+Utilities.m
//  Writeability
//
//  Created by Ryan on 5/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "NSArray+Utilities.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSArray+Shuffled
#pragma mark -
//*********************************************************************************************************************//




@implementation NSArray (Shuffled)

#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic shuffledCopy;

- (NSArray *)shuffledCopy
{
    return [self.shuffledMutableCopy copy];
}

@dynamic shuffledMutableCopy;

- (NSMutableArray *)shuffledMutableCopy
{
    NSMutableArray  *shuffled   = [self mutableCopy];
    NSUInteger      index       = [self count]-1;

    while(index) {
        [shuffled exchangeObjectAtIndex:index withObjectAtIndex:arc4random_uniform(index)];
        index --;
    }

    return shuffled;
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSArray+String
#pragma mark -
//*********************************************************************************************************************//




@implementation NSArray (String)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSString *)join
{
    return [self componentsJoinedByString:@""];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSArray+Set
#pragma mark -
//*********************************************************************************************************************//




@implementation NSArray (Set)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSSet *)uniqueValues
{
    return [NSSet setWithArray:self];
}

@end
