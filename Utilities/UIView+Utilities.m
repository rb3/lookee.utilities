//
//  UIView+Borders.m
//
//  Created by Aaron Ng on 12/28/13.
//  Copyright (c) 2013 Delve. All rights reserved.
//

#import "UIView+Utilities.h"

#import "LMacros.h"

#import "NSObject+Utilities.h"





//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIView+Borders
#pragma mark -
//*********************************************************************************************************************//





@implementation UIView(Borders)

#pragma mark - Static Objects
//*********************************************************************************************************************//

static NSString *const kUIViewBordersTopBorderTag       = @"__topBorder";
static NSString *const kUIViewBordersRightBorderTag     = @"__rightBorder";
static NSString *const kUIViewBordersBottomBorderTag    = @"__bottomBorder";
static NSString *const kUIViewBordersLeftBorderTag      = @"__leftBorder";

static inline CGRect UIViewBordersTopBorderFrame(CGRect frame, CGSize size) {
    return ((CGRect) {{0., 0.}, {frame.size.width, size.height}});
}

static inline CGRect UIViewBordersRightBorderFrame(CGRect frame, CGSize size) {
    return ((CGRect) {{frame.size.width - size.width, 0.}, {size.width, frame.size.height}});
}

static inline CGRect UIViewBordersBottomBorderFrame(CGRect frame, CGSize size) {
    return ((CGRect) {{0., frame.size.height - size.height}, {frame.size.width, size.height}});
}

static inline CGRect UIViewBordersLeftBorderFrame(CGRect frame, CGSize size) {
    return ((CGRect) {{0., 0.}, {size.width, frame.size.height}});
}



#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic topBorderWidth;

- (CGFloat)topBorderWidth
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersTopBorderTag];

    if (layer) {
        return (layer.frame.size.height);
    }

    return 0.;
}

- (void)setTopBorderWidth:(CGFloat)topBorderWidth
{
    CGSize size = {topBorderWidth, topBorderWidth};

    [self borderLayerWithTag:kUIViewBordersTopBorderTag sizingFunction:&UIViewBordersTopBorderFrame size:size];
}

@dynamic topBorderColor;

- (UIColor *)topBorderColor
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersTopBorderTag];

    if (layer) {
        return [UIColor colorWithCGColor:layer.backgroundColor];
    }

    return nil;
}

- (void)setTopBorderColor:(UIColor *)topBorderColor
{
    CALayer *layer
    =
    [self borderLayerWithTag:kUIViewBordersTopBorderTag sizingFunction:&UIViewBordersTopBorderFrame];

    [layer setBackgroundColor:topBorderColor.CGColor];
}

@dynamic rightBorderWidth;

- (CGFloat)rightBorderWidth
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersRightBorderTag];

    if (layer) {
        return (layer.frame.size.width);
    }

    return 0.;
}

- (void)setRightBorderWidth:(CGFloat)rightBorderWidth
{
    CGSize size = {rightBorderWidth, rightBorderWidth};

    [self borderLayerWithTag:kUIViewBordersRightBorderTag sizingFunction:&UIViewBordersRightBorderFrame size:size];
}

@dynamic rightBorderColor;

- (UIColor *)rightBorderColor
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersRightBorderTag];

    if (layer) {
        return [UIColor colorWithCGColor:layer.backgroundColor];
    }

    return nil;
}

- (void)setRightBorderColor:(UIColor *)rightBorderColor
{
    CALayer *layer
    =
    [self borderLayerWithTag:kUIViewBordersRightBorderTag sizingFunction:&UIViewBordersRightBorderFrame];

    [layer setBackgroundColor:rightBorderColor.CGColor];
}

@dynamic bottomBorderWidth;

- (CGFloat)bottomBorderWidth
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersBottomBorderTag];

    if (layer) {
        return (layer.frame.size.height);
    }

    return 0.;
}

- (void)setBottomBorderWidth:(CGFloat)bottomBorderWidth
{
    CGSize size = {bottomBorderWidth, bottomBorderWidth};

    [self borderLayerWithTag:kUIViewBordersBottomBorderTag sizingFunction:&UIViewBordersBottomBorderFrame size:size];
}

@dynamic bottomBorderColor;

- (UIColor *)bottomBorderColor
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersBottomBorderTag];

    if (layer) {
        return [UIColor colorWithCGColor:layer.backgroundColor];
    }

    return nil;
}

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor
{
    CALayer *layer
    =
    [self borderLayerWithTag:kUIViewBordersBottomBorderTag sizingFunction:&UIViewBordersBottomBorderFrame];

    [layer setBackgroundColor:bottomBorderColor.CGColor];
}

@dynamic leftBorderWidth;

- (CGFloat)leftBorderWidth
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersLeftBorderTag];

    if (layer) {
        return (layer.frame.size.width);
    }

    return 0.;
}

- (void)setLeftBorderWidth:(CGFloat)leftBorderWidth
{
    CGSize size = {leftBorderWidth, leftBorderWidth};

    [self borderLayerWithTag:kUIViewBordersLeftBorderTag sizingFunction:&UIViewBordersLeftBorderFrame size:size];
}

@dynamic leftBorderColor;

- (UIColor *)leftBorderColor
{
    CALayer *layer = [self findBorderLayerWithTag:kUIViewBordersLeftBorderTag];

    if (layer) {
        return [UIColor colorWithCGColor:layer.backgroundColor];
    }

    return nil;
}

- (void)setLeftBorderColor:(UIColor *)leftBorderColor
{
    CALayer *layer
    =
    [self borderLayerWithTag:kUIViewBordersLeftBorderTag sizingFunction:&UIViewBordersLeftBorderFrame];

    [layer setBackgroundColor:leftBorderColor.CGColor];
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (CALayer *)findBorderLayerWithTag:(NSString *)tag
{
    CALayer *borderLayer;

    for (CALayer *layer in [self.layer sublayers]) {
        if ([layer.name isEqualToString:tag]) {
            borderLayer = layer;
            break;
        }
    }

    return borderLayer;
}

- (CALayer *)borderLayerWithTag:(NSString *)tag sizingFunction:(CGRect(*)(CGRect, CGSize))function
{
    return [self borderLayerWithTag:tag sizingFunction:function size:CGSizeZero];
}

- (CALayer *)borderLayerWithTag:(NSString *)tag sizingFunction:(CGRect(*)(CGRect, CGSize))function size:(CGSize)size
{
    CALayer *borderLayer = [self findBorderLayerWithTag:tag];

    if (!borderLayer) {
        borderLayer = [CALayer new];
        [borderLayer setName:tag];

        $weakify(self) {
            [self.layer observerForKeyPath:@"bounds"] (^{
                $strongify(self) {
                    [borderLayer setFrame:function(self.layer.frame, borderLayer.frame.size)];
                }
            });
        }

        [self.layer addSublayer:borderLayer];
    }

    if (!CGSizeEqualToSize(size, CGSizeZero)) {
        [borderLayer setFrame:function(self.layer.frame, size)];
    }

    return borderLayer;
}

@end
