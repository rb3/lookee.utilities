//
//  UIView+Borders.h
//
//  Created by Aaron Ng on 12/28/13.
//  Copyright (c) 2013 Delve. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIView (Borders)

@property (nonatomic, readwrite, assign) CGFloat topBorderWidth;
@property (nonatomic, readwrite, strong) UIColor *topBorderColor;

@property (nonatomic, readwrite, assign) CGFloat rightBorderWidth;
@property (nonatomic, readwrite, strong) UIColor *rightBorderColor;

@property (nonatomic, readwrite, assign) CGFloat bottomBorderWidth;
@property (nonatomic, readwrite, strong) UIColor *bottomBorderColor;

@property (nonatomic, readwrite, assign) CGFloat leftBorderWidth;
@property (nonatomic, readwrite, strong) UIColor *leftBorderColor;

@end
