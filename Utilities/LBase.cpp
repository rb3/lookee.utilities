//
//  LBase.cpp
//  Writeability
//
//  Created by Ryan on 6/5/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#include "LBase.h"

#include "base58.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LBase
#pragma mark -
//*********************************************************************************************************************//



#pragma mark - Public Functions
//*********************************************************************************************************************//

char * LBase58Encode(const unsigned char *encode, size_t length, size_t *encodedLength) {
    std::string result = EncodeBase58(std::vector<unsigned char>(encode, encode + length));

    if (encodedLength) {
        *encodedLength = result.length();
    }

    return strdup(result.c_str());
}

unsigned char * LBase58Decode(const char *decode, size_t length, size_t *decodedLength) {
    std::vector<unsigned char> vector;

    bool result = DecodeBase58(std::string(decode, length), vector);

    if (result) {
        if (decodedLength) {
            *decodedLength = vector.size();
        }

        return (unsigned char *)strdup((char *)&vector[0]);
    }
    
    return NULL;
}
