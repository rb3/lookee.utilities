//
//  LPDF.h
//  Writeability
//
//  Created by Ryan on 9/20/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//



#ifdef __cplusplus
extern "C" {
#endif

    
#import <CoreGraphics/CGContext.h>
#import <CoreGraphics/CGBitmapContext.h>
#import <CoreGraphics/CGPDFPage.h>

#import <UIKit/UIGraphics.h>
#import <UIKit/UIColor.h>



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPDF
#pragma mark -
//*********************************************************************************************************************//



#pragma mark - Public Methods
//*********************************************************************************************************************//

static inline void LPDFPageDimensions(CGPDFPageRef pageRef, CGSize *dimensions) {
    CGPDFPageRetain(pageRef);

    CGRect 	cropBoxRect 	= CGPDFPageGetBoxRect(pageRef, kCGPDFCropBox);
    CGRect 	mediaBoxRect 	= CGPDFPageGetBoxRect(pageRef, kCGPDFMediaBox);
    CGRect 	effectiveRect 	= CGRectIntersection(cropBoxRect, mediaBoxRect);

    NSUInteger 	pageAngle 		= CGPDFPageGetRotationAngle(pageRef);
    CGFloat 	pageWidth		= 0.;
    CGFloat 	pageHeight		= 0.;

    switch (pageAngle) {
        default:
        case 0:
        case 180: {
            pageWidth	= effectiveRect.size.width;
            pageHeight 	= effectiveRect.size.height;
        } break;
        case 90:
        case 270: {
            pageWidth 	= effectiveRect.size.height;
            pageHeight 	= effectiveRect.size.width;
        } break;
    }

    NSUInteger intPageWidth 	= (NSUInteger)(pageWidth);
    NSUInteger intPageHeight 	= (NSUInteger)(pageHeight);

    if (intPageWidth %2) {
        intPageWidth --;
    }

    if (intPageHeight %2) {
        intPageHeight --;
    }

    if (dimensions) {
        dimensions->width  = intPageWidth;
        dimensions->height = intPageHeight;
    }

    CGPDFPageRelease(pageRef);
}

static inline CGAffineTransform LPDFPageTransform(CGPDFPageRef pageRef) {
    CGSize dimensions;
    LPDFPageDimensions(pageRef, &dimensions);

    CGAffineTransform   transform   = CGAffineTransformIdentity;

    CGRect              bounds		= ((CGRect) {
        .origin	= CGPointZero,
        .size	= dimensions
    });

    transform = CGAffineTransformConcat(transform, CGPDFPageGetDrawingTransform(pageRef,
                                                                                kCGPDFCropBox,
                                                                                bounds,
                                                                                0,
                                                                                YES));

    return transform;
}

static inline void LPDFInfo
(
 NSURL		*URL,
 NSUInteger *pageInOut,
 CGSize     *dimensions
 ) {
	NSUInteger pageCount    = 0;
    NSUInteger pageNumber   = 1;

	assert(URL);
	{
		CGPDFDocumentRef documentRef = CGPDFDocumentCreateWithURL((__bridge CFURLRef)(URL));

		if (documentRef) {
			pageCount = CGPDFDocumentGetNumberOfPages(documentRef);

            if (pageInOut) {
                if (*pageInOut < 1) {
                    *pageInOut = pageCount;
                } else {
                    pageNumber = *pageInOut;
                }
            }

			if (dimensions) {
				CGPDFPageRef pageRef = CGPDFDocumentGetPage(documentRef, pageNumber);

				if (pageRef) {
					LPDFPageDimensions(pageRef, dimensions);
				}
            }

			CGPDFDocumentRelease(documentRef);
		}
	}
}

static inline BOOL LPDFIsValid(NSURL *URL)
{
    CGSize dimensions = CGSizeZero;
    LPDFInfo(URL, NULL, &dimensions);

    return !CGSizeEqualToSize(dimensions, CGSizeZero);
}

static inline void LPDFPageRenderToContext
(CGPDFPageRef       page,
 CGContextRef       context,
 CGAffineTransform  applyTransform,
 BOOL               scaleToContext)
{
    CGAffineTransform   transform   = CGContextGetCTM(context);

    float               width       = roundf(CGBitmapContextGetWidth(context)  / fabsf(transform.a));
    float               height      = roundf(CGBitmapContextGetHeight(context) / fabsf(transform.d));

    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetRenderingIntent(context, kCGRenderingIntentDefault);

    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    CGContextFillRect(context, ((CGRect) {{0}, {width, height}}));

    CGContextTranslateCTM(context, 0., height);
    CGContextScaleCTM(context, 1., -1.);

    CGContextConcatCTM(context, applyTransform);

    // This scales stretched images to the correct size
    if (scaleToContext) {
        CGSize dimensions;

        LPDFPageDimensions(page, &dimensions);

        if (isgreater(dimensions.width , width) ||
            isgreater(dimensions.height, height)) {
            float scale = fminf((width  / dimensions.width),
                                (height / dimensions.height));

            CGContextScaleCTM(context, scale, scale);
        }
    }

    CGContextDrawPDFPage(context, page);
}

static inline BOOL LPDFDocumentRenderToContext(CGPDFDocumentRef document, NSUInteger pageNumber, CGContextRef context)
{
    CGPDFPageRef pageRef = CGPDFDocumentGetPage(document, pageNumber);

    if (pageRef) {
        LPDFPageRenderToContext(pageRef, context, LPDFPageTransform(pageRef), YES);
        
        return YES;
    }
    
    return NO;
}

static inline BOOL LPDFRenderToContext(NSURL *URL, NSUInteger pageNumber, CGContextRef context) {
    CGPDFDocumentRef document = CGPDFDocumentCreateWithURL((__bridge CFURLRef)(URL));

    if (document) {
        BOOL success = LPDFDocumentRenderToContext(document, pageNumber, context);

        CGPDFDocumentRelease(document);

        return success;
    }

    return NO;
}

static inline UIImage *LPDFPageSnapshot
(
 NSURL		*URL,
 NSUInteger	pageNumber,
 NSUInteger width,
 NSUInteger height,
 NSUInteger	scale)
{
    CGSize          size        ; LPDFInfo(URL, &pageNumber, &size);
    float           sizeScale   = fminf((width / size.width), (height / size.height));

    size.width  *= sizeScale;
    size.height *= sizeScale;

	UIGraphicsBeginImageContextWithOptions(size, YES, scale);

    UIImage         *image      = nil;
	CGContextRef    context     = UIGraphicsGetCurrentContext();

	if (context) {
		if (LPDFRenderToContext(URL, pageNumber, context)) {
            image = UIGraphicsGetImageFromCurrentImageContext();
        }

        UIGraphicsEndImageContext();
	}
    
	return image;
}

#ifdef __cplusplus
}
#endif