//
//  UIColor+Utilities.m
//  Writeability
//
//  Created by Ryan on 8/25/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "UIColor+Utilities.h"

#import <Utilities/NSString+Utilities.h>

#import <OpenGLES/ES2/glext.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIColor+Compare
#pragma mark -
//*********************************************************************************************************************//




@implementation UIColor (Compare)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (BOOL)isEqualToColor:(UIColor *)color
{
    return CGColorEqualToColor([self CGColor], [color CGColor]);
}

- (BOOL)isLight
{
    CGFloat const *components   = CGColorGetComponents([self CGColor]);

    CGFloat const brightness    = ((components[0] *299.) + (components[1] *587.) + (components[2] *114.)) /1000.;

    return isgreaterequal(brightness, .5);
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIColor+Components
#pragma mark -
//*********************************************************************************************************************//




@implementation UIColor (Components)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)getComponents:(float *)rgba
{
    CGColorSpaceModel	model		= CGColorSpaceGetModel(CGColorGetColorSpace(self.CGColor));
    const CGFloat		*components	= CGColorGetComponents(self.CGColor);

    switch (model) {
        case kCGColorSpaceModelMonochrome: {
            rgba[0] = components[0];
            rgba[1] = components[0];
            rgba[2] = components[0];
            rgba[3] = components[1];
        } break;
        case kCGColorSpaceModelRGB: {
            rgba[0] = components[0];
            rgba[1] = components[1];
            rgba[2] = components[2];
            rgba[3] = components[3];
        } break;
        default: {
            rgba[0] = 0.;
            rgba[1] = 0.;
            rgba[2] = 0.;
            rgba[3] = 1.;
        } break;
    }
}

- (float)red
{
    float rgba[4];

    [self getComponents:rgba];

    return rgba[0];
}

- (float)green
{
    float rgba[4];

    [self getComponents:rgba];

    return rgba[1];
}

- (float)blue
{
    float rgba[4];

    [self getComponents:rgba];

    return rgba[2];
}

- (float)alpha
{
    float rgba[4];

    [self getComponents:rgba];
    
    return rgba[3];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIColor+GL
#pragma mark -
//*********************************************************************************************************************//




@implementation UIColor (GL)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)bindGLClearColor
{
    GLfloat rgba[4];

    [self getComponents:rgba];

    glClearColor(rgba[0] * rgba[3],
				 rgba[1] * rgba[3],
				 rgba[2] * rgba[3],
				 rgba[3]);
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark UIColor+Hex
#pragma mark -
//*********************************************************************************************************************//




@implementation UIColor (HexString)

#pragma mark - Static Functions
//*********************************************************************************************************************//

static inline float getRGBFromHexString(NSString *hexString, float *red, float *green, float *blue) {
    NSString *cleanString = [hexString stringByDeletingSubstring:@"#"];

    if ([cleanString length] == 3) {
        cleanString = [NSString stringWithFormat:@"%@%@%@%@%@%@",
                       [cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(0, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(1, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)],
                       [cleanString substringWithRange:NSMakeRange(2, 1)]];
    }

    if ([cleanString length] == 6) {
        cleanString = [cleanString stringByAppendingString:@"ff"];
    }

    unsigned int baseValue; [[NSScanner scannerWithString:cleanString] scanHexInt:&baseValue];

    *red    = ((baseValue >> 24)    & 0xFF) /255.;
    *green  = ((baseValue >> 16)    & 0xFF) /255.;
    *blue   = ((baseValue >> 8)     & 0xFF) /255.;

    return ((baseValue >> 0) & 0xFF) /255.;
}


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)colorFromHexString:(NSString *)hexString
{
    float red, green, blue, alpha = getRGBFromHexString(hexString, &red, &green, &blue);

    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithHexString:(NSString *)hexString
{
    float red, green, blue, alpha = getRGBFromHexString(hexString, &red, &green, &blue);

    if ((self = [self initWithRed:red green:green blue:blue alpha:alpha])) {

    }

    return self;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSString *)hexString
{
    CGFloat components[4]; [self getComponents:components];

    return [NSString stringWithFormat:@"%02lX%02lX%02lX%02lX",
            lroundf(components[0] *255),
            lroundf(components[1] *255),
            lroundf(components[2] *255),
            lroundf(components[3] *255)];
}

@end
