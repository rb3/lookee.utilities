//
//  LFastTrig.h
//  Writeability
//
//  Created by Ryan on 10/2/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Accelerate/Accelerate.h>

#import <Foundation/Foundation.h>


#ifdef __cplusplus
extern "C" {
#endif



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LFastMath
#pragma mark -
//*********************************************************************************************************************//




#pragma mark - Public Methods
//*********************************************************************************************************************//

static inline float LFMDivf(const float dividend, const float divisor) {
    assert(isfinite(dividend));
    assert(isfinite(divisor));

	int		count = 1;
	float	result;

	vvdivf(&result, &dividend, &divisor, &count);

    assert(isfinite(result));
	return result;
}

static inline float LFMPowf(const float value, const float exponent) {
    assert(isfinite(value));
    assert(isfinite(exponent));

	int		count = 1;
	float	result;

	vvpowf(&result, &exponent, &value, &count);

    assert(isfinite(result));
	return result;
}

static inline float LFMSqrtf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvsqrtf(&result, &value, &count);

    assert(isfinite(result));
	return result;
}

static inline float LFMCeilf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvceilf(&result, &value, &count);

	return result;
}

static inline float LFMFloorf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;
    
    vvfloorf(&result, &value, &count);

	return result;
}

static inline float LFMRoundf(const float value) {
    assert(isfinite(value));

    int		count = 1;
	float	result;

    vvnintf(&result, &value, &count);

	return result;
}

static inline float LFMFabsf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvfabsf(&result, &value, &count);

	return result;
}

static inline float LFMCosf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvcosf(&result, &value, &count);

	return result;
}

static inline float LFMCospif(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvcospif(&result, &value, &count);

	return result;
}

static inline float LFMSinf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvsinf(&result, &value, &count);

	return result;
}

static inline float LFMSinpif(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvsinpif(&result, &value, &count);

	return result;
}

static inline float LFMTanf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvtanf(&result, &value, &count);

    assert(isfinite(result));
	return result;
}

static inline float LFMAtan2f(const float dividend, const float divisor) {
    assert(isfinite(dividend));
    assert(isfinite(divisor));

	int		count = 1;
	float	result;

	vvatan2f(&result, &dividend, &divisor, &count);

    assert(isfinite(result));
	return result;
}

static inline float LFMLog2f(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvlog2f(&result, &value, &count);

    assert(isfinite(result));
	return result;
}

static inline float LFMRecf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvrecf(&result, &value, &count);

    assert(isfinite(result));
	return result;
}

static inline int LFMNintf(const float value) {
    assert(isfinite(value));

	int		count = 1;
	float	result;

	vvnintf(&result, &value, &count);

	return result;
}

static inline float LFMLinearInterpolate(const float from, const float to, const float factor) {
    assert(isfinite(from));
    assert(isfinite(to));
    assert(isfinite(factor));

    return (from + (to - from) * factor);
}

static inline float LFMReverseLinearInterpolate(const float from, const float to, const float value) {
    assert(isfinite(from));
    assert(isfinite(to));
    assert(isfinite(value));

    return ((value - from) / (to - from));
}

static inline float LFMCosineInterpolate(const float from, const float to, const float factor) {
    assert(isfinite(from));
    assert(isfinite(to));
    assert(isfinite(factor));

    return LFMLinearInterpolate(from, to, LFMDivf((-LFMCospif(factor) +1.), 2.));
}

static inline float LFMNextPowerOfTwo(const float value) {
    assert(isfinite(value));

    unsigned long ceiling = LFMNintf(value);

    -- ceiling;

    ceiling |= ceiling >> 1;
    ceiling |= ceiling >> 2;
    ceiling |= ceiling >> 4;
    ceiling |= ceiling >> 8;
    ceiling |= ceiling >> 16;

    ++ ceiling;

    return ceiling;
}

static inline float LFMNearestPerfectSquare(const float value) {
    assert(isfinite(value));

    return LFMPowf(LFMRoundf(LFMSqrtf(value)), 2.);
}

static inline float LFMFractional(const float value) {
    assert(isfinite(value));

    float result;

    vDSP_vfrac(&value, 1, &result, 1, 1);

    return result;
}

static inline void LFMFloora(float *result, const float *value, const int count) {
    vvfloorf(result, value, &count);
}

static inline void LFMRounda(float *result, const float *value, const int count) {
    vvnintf(result, value, &count);
}

    
#ifdef __cplusplus
}
#endif

