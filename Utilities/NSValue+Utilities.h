//
//  NSValue+Utilities.h
//  Utilities
//
//  Created by Ryan Blonna on 15/1/15.
//  Copyright (c) 2015 Lookee. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSValue (Boxed)

+ (instancetype)valueFromBoxedRepresentation:(id)representation;


- (instancetype)initWithBoxedRepresentation:(id)representation;


- (id)boxedRepresentation;

@end
