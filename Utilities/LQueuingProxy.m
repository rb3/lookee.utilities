//
//  LQueuingProxy.m
//  Writeability
//
//  Created by Ryan on 7/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LQueuingProxy.h"

#import "NSOperationQueue+Utilities.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//





@interface LAsynchronousProxy : LQueuingProxy

@end


@interface LSynchronousProxy : LQueuingProxy

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LQueuingProxy
#pragma mark -
//*********************************************************************************************************************//




@interface LQueuingProxy ()

- (void)forwardAsynchronousInvocation:(NSInvocation *)invocation;
- (void)forwardSynchronousInvocation:(NSInvocation *)invocation;

@end

@implementation LQueuingProxy

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@protected
    NSOperationQueue *queue;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//


+ (id)proxyWithObject:(id)object
{
    return [self proxyWithObject:object queue:[NSOperationQueue new]];
}

+ (id)proxyWithObject:(id)object queue:(NSOperationQueue *)queue
{
    LQueuingProxy *instance = [self initializeProxyWithObject:object queue:queue];
    {
        instance->$desynchronize    = [LAsynchronousProxy initializeProxyWithObject:object queue:queue];
        instance->$synchronize      = [LSynchronousProxy initializeProxyWithObject:object queue:queue];
    }

    return instance;
}

+ (id)initializeProxyWithObject:(id)object queue:(NSOperationQueue *)queue
{
    LQueuingProxy *instance = [super proxyWithObject:object];
    {
        instance->queue = queue;

        [queue setMaxConcurrentOperationCount:1];
    }

    return instance;
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize $desynchronize  = $desynchronize;
@synthesize $synchronize    = $synchronize;


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (id)forwardingTargetForSelector:(SEL)selector
{
    return nil;
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    if (strcmp(invocation.methodSignature.methodReturnType, @encode(void)) == 0) {
        [self forwardAsynchronousInvocation:invocation];
    } else {
        [self forwardSynchronousInvocation:invocation];
    }
}


#pragma mark - Protected Methods
//*********************************************************************************************************************//

- (void)forwardAsynchronousInvocation:(NSInvocation *)invocation
{
    [super forwardInvocation:invocation];

    [queue addSerialOperation:[[NSInvocationOperation alloc] initWithInvocation:invocation]];
}

- (void)forwardSynchronousInvocation:(NSInvocation *)invocation
{
    [super forwardInvocation:invocation];

    [queue addSynchronousOperation:[[NSInvocationOperation alloc] initWithInvocation:invocation]];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LAsynchronousProxy

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)forwardInvocation:(NSInvocation *)invocation
{
    [super forwardAsynchronousInvocation:invocation];
}

@end

@implementation LSynchronousProxy

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (void)forwardInvocation:(NSInvocation *)invocation
{
    [super forwardSynchronousInvocation:invocation];
}

@end