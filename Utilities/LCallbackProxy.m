//
//  LCallbackProxy.m
//  Writeability
//
//  Created by Ryan on 7/31/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LCallbackProxy.h"

#import "LMacros.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LCallbackProxy
#pragma mark -
//*********************************************************************************************************************//





@implementation LCallbackProxy

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (id)proxy __ILLEGALMETHOD__;
+ (id)proxyWithObject:(id)object __ILLEGALMETHOD__;

+ (id)proxyWithType:(Class)class
{
    LCallbackProxy *instance = [super proxy];
    {
        instance->object = class;
    }

    return instance;
}


#pragma mark - Properties
//*********************************************************************************************************************//

@synthesize $callback = $callback;


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (Class)class
{
    return(id)( object );
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %p> (proxy)", $classname, self];
}

- (BOOL)respondsToSelector:(SEL)selector
{
    return [[self class] instancesRespondToSelector:selector];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    return [[self class] instanceMethodSignatureForSelector:selector];
}

- (id)forwardingTargetForSelector:(SEL)selector
{
    return nil;
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    DBGAssert($callback != nil);

    [self $callback](invocation);
}

@end
