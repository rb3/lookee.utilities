//
//  LSurrogateProxy.h
//  Writeability
//
//  Created by Ryan on 8/8/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LProxy.h"


@interface LSurrogateProxy : LProxy

+ (id)proxyWithType:(Class)type;


- (void)$setObject:(id)instance;

@end


@interface NSObject (LSurrogateProxy)

- (void)$setObject:(id)instance;

@end