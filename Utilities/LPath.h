//
//  LPaths.h
//  Writeability
//
//  Created by Ryan on 10/9/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#ifdef __cplusplus
extern "C" {
#endif



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LPath
#pragma mark -
//*********************************************************************************************************************//



#pragma mark - Public Methods
//*********************************************************************************************************************//

static inline NSString *LPathDocuments() {
	return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

static inline NSString *LPathCache()
{
    return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

static inline NSString *LPathTemporary() {
    return [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSProcessInfo processInfo] globallyUniqueString]];
}

static inline NSString * LPathForFileWithName(NSString *fileName) {
    NSString *extension = [fileName pathExtension];
    NSString *base      = [fileName stringByDeletingPathExtension];

    return [[NSBundle mainBundle] pathForResource:base ofType:extension];
}


#ifdef __cplusplus
}
#endif