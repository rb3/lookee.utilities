//
//  NSThread+Block.m
//  Writeability
//
//  Created by Ryan on 10/27/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "NSThread+Block.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSThread+Block
#pragma mark -
//*********************************************************************************************************************//





@implementation NSThread (Block)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (void)performBlockOnMainThread:(void(^)(void))block
{
	[[NSThread mainThread] performBlock:block];
}

+ (void)performBlockInBackground:(void(^)(void))block
{
	[NSThread performSelectorInBackground:@selector(runBlock:) withObject:block];
}

+ (void)runBlock:(void(^)(void))block
{
	block();
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)performBlock:(void(^)(void))block
{
	[self performBlock:block waitUntilDone:NO];
}

- (void)performBlock:(void(^)(void))block waitUntilDone:(BOOL)wait
{
    if ([NSThread currentThread] == self) {
        block();
    } else {
        [NSThread performSelector:@selector(runBlock:) onThread:self withObject:block waitUntilDone:wait];
    }
}

- (void)performBlock:(void(^)(void))block afterDelay:(NSTimeInterval)delay
{
	[self performSelector:@selector(performBlock:) withObject:block afterDelay:delay];
}

@end
