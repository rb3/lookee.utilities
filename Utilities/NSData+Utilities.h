//
//  NSData+Utilities.h
//  Writeability
//
//  Created by Ryan Blonna on 14/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface NSData (Representation)

+ (instancetype)dataFromBase58Representation:(NSString *)representation;
+ (instancetype)dataFromBase64Representation:(NSString *)representation;


- (instancetype)initWithBase58Representation:(NSString *)representation;
- (instancetype)initWithBase64Representation:(NSString *)representation;


- (NSString *)base58Representation;
- (NSString *)base64Representation;

@end


@interface NSData (NSValue)

+ (instancetype)dataWithValue:(NSValue *)value;
+ (instancetype)dataWithCGAffineTransform:(CGAffineTransform)transform;
+ (instancetype)dataWithCGPoint:(CGPoint)point;
+ (instancetype)dataWithCGRect:(CGRect)rect;
+ (instancetype)dataWithCGSize:(CGSize)size;
+ (instancetype)dataWithUIEdgeInsets:(UIEdgeInsets)insets;
+ (instancetype)dataWithUIOffset:(UIOffset)offset;


- (CGAffineTransform)CGAffineTransformValue;
- (CGPoint          )CGPointValue;
- (CGRect           )CGRectValue;
- (CGSize           )CGSizeValue;
- (UIEdgeInsets     )UIEdgeInsetsValue;
- (UIOffset         )UIOffsetValue;

@end