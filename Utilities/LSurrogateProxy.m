//
//  LSurrogateProxy.m
//  Writeability
//
//  Created by Ryan on 8/8/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LSurrogateProxy.h"

#import "LMacros.h"

#import "NSThread+Block.h"





//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LSurrogateAction : NSObject

@property (nonatomic, readonly, strong) NSThread        *thread;
@property (nonatomic, readonly, strong) NSInvocation    *action;


+ (instancetype)surrogateWithAction:(NSInvocation *)action;


- (void)invokeWithTarget:(id)target;
- (void)invoke;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LSurrogateProxy
#pragma mark -
//*********************************************************************************************************************//




@implementation LSurrogateProxy

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    Class           class;
    NSMutableArray  *invocations;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (id)proxy __ILLEGALMETHOD__;

+ (id)proxyWithObject:(id)object
{
    LSurrogateProxy *instance = [super proxyWithObject:object];
    {
        instance->class         = [object class];
        instance->invocations   = [NSMutableArray new];
    }

    return instance;
}

+ (id)proxyWithType:(Class)class
{
    LSurrogateProxy *instance = [super proxy];
    {
        instance->class         = class;
        instance->invocations   = [NSMutableArray new];
    }

    return instance;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (void)$setObject:(id)instance
{
    @synchronized(objectLock) {
        object = instance;
    }

    NSArray *actions;

    @synchronized(invocations) {
        if (instance) {
            actions = [invocations copy];
        }

        [invocations removeAllObjects];
    }

    if (instance) {
        for (LSurrogateAction *action in actions) {
            [action invokeWithTarget:instance];
        }
    }
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (Class)class
{
	return (id)(class);
}

- (NSString *)description
{
    NSString *description;

    @synchronized(objectLock) {
        description = [super description];
    }

    if (!description) {
        description = [NSString stringWithFormat:@"<%@: %p> (proxy)", $classname, self];
    }

    return description;
}

- (BOOL)respondsToSelector:(SEL)selector
{
    return [[self class] instancesRespondToSelector:selector];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    return [[self class] instanceMethodSignatureForSelector:selector];
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    [super forwardInvocation:invocation];

    @synchronized(invocations) {
        [invocations addObject:[LSurrogateAction surrogateWithAction:invocation]];
    }
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Implementations
#pragma mark -
//*********************************************************************************************************************//




@implementation LSurrogateAction

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)surrogateWithAction:(NSInvocation *)action
{
    return [[self alloc] initWithAction:action];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithAction:(NSInvocation *)action
{
    if ((self = [super init])) {
        _thread = $currentthread;
        _action = action;

        [self initialize];
    }

    return self;
}

- (void)initialize
{
    NSMutableArray *retainer = [NSMutableArray new];

    if (SYSTEM_VERSION_LESS_THAN(7.0)) {
        /*
         * HACK for iOS 6 and below due to Apple bug:
         * Blocks are not copied when -retainArguments
         * is called on NSInvocation.
         */

        NSUInteger count = [self.action.methodSignature numberOfArguments];

        for (NSUInteger index = 2; index < count; index ++) {
            const char *type = [self.action.methodSignature getArgumentTypeAtIndex:index];

            if (strstr(type, "@?") != nil) {
                __unsafe_unretained
                id argument ; [self.action getArgument:&argument atIndex:index];
                id block    = [argument copy];

                [retainer addObject:block];

                [self.action setArgument:&block atIndex:index];
            }
        }
    }

    [self.action retainArguments];
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)invokeWithTarget:(id)target
{
    $weakify(self) {
        [self.thread performBlock:^{
            $strongify(self) {
                [self.action invokeWithTarget:target];
            }
        } waitUntilDone:YES];
    }
}

- (void)invoke
{
    [self invokeWithTarget:self.action.target];
}

@end