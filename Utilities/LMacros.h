//
//  LMacros.h
//  Writeability
//
//  Created by Ryan on 8/23/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LMacros
#pragma mark This file declares and defines constants and convenience macros, as well as some inline functions.
#pragma mark Note that macros prefixed with "$__" are not meant to be used outside of this file.
#pragma mark -
//*********************************************************************************************************************//




#pragma mark - Constants
//*********************************************************************************************************************//

/**
 * This ensures that DEBUG is defined.
 */
#ifndef DEBUG
#   define DEBUG 1
#endif

/**
 * These macros can be set to either silence 
 * NSLog, or silence DBG logging macros.
 */
#define DEBUG_SILENCENSLOG 0
#define DEBUG_SILENT 0


#pragma mark - Token Manipulation
//*********************************************************************************************************************//

/**
 * This macro makes it easier to use inline pragmas, so 
 * you can type $_pragma(<expression>) without having to 
 * worry about escaping quotes within the expression.
 * Inline pragmas can be used anywhere in the code - even
 * in macros. Examples of its use can be found in this file.
 */
#pragma mark [ # ] $_pragma(<args...>)
#define $_pragma(args...) _Pragma($_quote(args))

/**
 * This is the standard stringification macro.
 * $_quote(<token>) expands to exactly "<token>".
 */
#pragma mark [ # ] $_quote(<token?>)
#define $_quote(token...) $___quote(token)
#	define $___quote(token...) #token

/**
 * This is the standard concatenation macro.
 * $_concat(<t1>, <t2>) expands to exactly <t1><t2>.
 */
#pragma mark [ # ] $_concat(<token>, <other>)
#define $_concat(token, other) $___concat(token, other)
#	define $___concat(token, other) token##other

/**
 * This macro generates a unique name. $_unique(<token>) 
 * expands to <token> with a unique integer appended to it. 
 * Note that uniqueness is guaranteed per file scope only.
 */
#pragma mark [ # ] $_unique(<token>)
#define $_unique(token) $_concat(token, __COUNTER__)

/**
 * This macro counts the number of comma-separated 
 * tokens in an expression. It can be used to get 
 * the number of arguments in a variadic macro. It 
 * can only count up to 16 tokens.
 */
#pragma mark [ # ] $_argc(<args...>)
#define $_argc(args...) $___argc(_, ##args, 16,15,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0)
#	define $___argc(args...) $___argcexp(args)
#   define $___argcexp(_, _0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15, N, ...) N

/**
 * This set of macros can be used to obtain the
 * (N+1)th token of a series of comma-separated
 * tokens. $_argat(2, <t1>, <t2>, <t3>) and
 * $_argat2(<t1>, <t2>, <t3>) expand to exactly
 * <t3>.
 */
#pragma mark [ # ] $_argat(<index>, <args...>)
#define $_argat(index, args...) $_concat($_argat, index)(args)

#pragma mark [ # ] $_argat<index>(<args...>)
#define $_argat0(_0, ...) _0
#define $_argat1(_0,_1, ...) _1
#define $_argat2(_0,_1,_2, ...) _2
#define $_argat3(_0,_1,_2,_3, ...) _3
#define $_argat4(_0,_1,_2,_3,_4, ...) _4
#define $_argat5(_0,_1,_2,_3,_4,_5, ...) _5
#define $_argat6(_0,_1,_2,_3,_4,_5,_6, ...) _6
#define $_argat7(_0,_1,_2,_3,_4,_5,_6,_7, ...) _7
#define $_argat8(_0,_1,_2,_3,_4,_5,_6,_7,_8, ...) _8
#define $_argat9(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9, ...) _9
#define $_argat10(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10, ...) _10
#define $_argat11(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11, ...) _11
#define $_argat12(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12, ...) _12
#define $_argat13(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13, ...) _13
#define $_argat14(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14, ...) _14
#define $_argat15(_0,_1,_2,_3,_4,_5,_6,_7,_8,_9,_10,_11,_12,_13,_14,_15, ...) _15

/**
 * This macro applies another macro on each token 
 * in a comma-separated, parenthesis-enclosed list. 
 * It is also possible to pass multiple context 
 * tokens and the index to the applied macro.
 * $_foreach(<macro>, (<t1>, <t2>), <c1>, <c2>) 
 * expands to exactly <macro>(0, <t1>, <c1>, <c2>)
 * <macro>(1, <t2>, <c1>, <c2>).
 */
#pragma mark [ # ] $_foreach(<macro>, <(list...)>, <context...>)
#define $_foreach(macro, list, context...) $_concat($___foreach, $_argc list)(macro, list, context)
#   define $___foreach0(macro, list, context...)
#   define $___foreach1(macro, list, context...) $___foreach0(macro, list, context) macro(0, $_argat0 list, context)
#   define $___foreach2(macro, list, context...) $___foreach1(macro, list, context) macro(1, $_argat1 list, context)
#   define $___foreach3(macro, list, context...) $___foreach2(macro, list, context) macro(2, $_argat2 list, context)
#   define $___foreach4(macro, list, context...) $___foreach3(macro, list, context) macro(3, $_argat3 list, context)
#   define $___foreach5(macro, list, context...) $___foreach4(macro, list, context) macro(4, $_argat4 list, context)
#   define $___foreach6(macro, list, context...) $___foreach5(macro, list, context) macro(5, $_argat5 list, context)
#   define $___foreach7(macro, list, context...) $___foreach6(macro, list, context) macro(6, $_argat6 list, context)
#   define $___foreach8(macro, list, context...) $___foreach7(macro, list, context) macro(7, $_argat7 list, context)
#   define $___foreach9(macro, list, context...) $___foreach8(macro, list, context) macro(8, $_argat8 list, context)
#   define $___foreach10(macro, list, context...) $___foreach9(macro, list, context) macro(9, $_argat9 list, context)
#   define $___foreach11(macro, list, context...) $___foreach10(macro, list, context) macro(10, $_argat10 list, context)
#   define $___foreach12(macro, list, context...) $___foreach11(macro, list, context) macro(11, $_argat11 list, context)
#   define $___foreach13(macro, list, context...) $___foreach12(macro, list, context) macro(12, $_argat12 list, context)
#   define $___foreach14(macro, list, context...) $___foreach13(macro, list, context) macro(13, $_argat13 list, context)
#   define $___foreach15(macro, list, context...) $___foreach14(macro, list, context) macro(14, $_argat14 list, context)
#   define $___foreach16(macro, list, context...) $___foreach15(macro, list, context) macro(15, $_argat15 list, context)

/**
 * This macro is used to remove enclosing parentheses 
 * from a token. If an empty argument is provided, it 
 * expands to nothing. $_deparenthesize((<t>)) expands 
 * to exactly <t>.
 */
#pragma mark [ # ] $_deparenthesize(<(arg)?>)
#define $_deparenthesize(arg) $_concat($___deparenthesize, $_argc(arg)) arg
#   define $___deparenthesize0
#   define $___deparenthesize1(args...) args

/**
 * These macros are used to convert any expression 
 * to 0 or 1. $_not(0) expands to 1, and if called 
 * with any other argument, it will expand to 0. 
 * $_bool does the logical opposite of $_not.
 */
#pragma mark [ # ] $_not(<expression>)
#define $_not(expression) $___nottest($_concat($___not, expression)())
#   define $___nottest(args...) $_argat1(args, 0)
#   define $___not() $___not0probe()
#   define $___not0() $___not0probe()
#       define $___not0probe() ~, 1

#pragma mark [ # ] $_bool(<expression>)
#define $_bool(expression) $_not($_not(expression))

/**
 * This macro generates a given expression depending
 * on the boolean value of a given condition.
 * $_if(1)(<true>)(<false>) will expand to <true>.
 * The unexpanded expression will not be evaluated.
 */
#pragma mark [ # ] $_if(<condition>)(<true-statement>)(<false-statement>)
#define $_if(condition) $___if($_bool(condition))
#   define $___if(condition) $_concat($___if, condition)
#       define $___if1(expression...) expression $___if1else
#           define $___if1else(expression...)
#       define $___if0(expression...) $___if0else
#           define $___if0else(expression...) expression

/**
 * This macro allows one to declare a variable visible 
 * only within a provided brace-enclosed scope.
 * $_declare(int var1, float var2) { <code> } will 
 * declare an integer var1 and float var2 accessible 
 * only from <code>.
 */
#pragma mark [ # ] $_declare(<declarations...>)
#define $_declare(declarations...)                      \
    $_pragma(clang diagnostic push)                     \
    $_pragma(clang diagnostic ignored "-Wgcc-compat")   \
    $_foreach($___declare, (declarations),)             \
    $_pragma(clang diagnostic pop)
#   define $___declare(_0, declaration, _2)             \
        for (declaration ;; ({ break; }))

/**
 * This macro allows one to run arbitrary parenthesized 
 * code before and after a given brace-enclosed scope. 
 * $_do(id, (<e1>), (<e2>)) { <code> } runs <e1> before 
 * and <e2> after <code>.
 */
#pragma mark [ # ] $_do(<macroid>, <(before)?>, <(after)?>)
#define $_do(macroid, before, after)                \
    if (YES) {                                      \
        $_deparenthesize(before);                   \
        goto $_concat(__macro_body_, macroid);      \
    } else                                          \
        while (YES)                                 \
            if (YES) {                              \
                $_deparenthesize(after);            \
                break;                              \
            } else $_concat(__macro_body_, macroid):

/**
 * The following macro can be used when creating
 * other macros to add syntactic sugar. Macro 
 * definitions with this included allow them to
 * called as @macro(). There are no performance
 * implications when using it.
 */
#define __KEYWORD__ $_if(DEBUG)(autoreleasepool {})(try {} @catch (...) {})


#pragma mark - Debugging
//*********************************************************************************************************************//

/**
 * These macros are the debug logging macros. They are 
 * all prefixed with DBG. Note that DBGError causes an
 * assertion when called, so it is recommended to enclose 
 * it in a conditional statement. DBGMark and DBGTrace 
 * cause Xcode to generate a visual message in its UI.
 */
#if !DEBUG || DEBUG_SILENT
#   define DBGMessage(format, args...)
#   define DBGError(format, args...)
#   define DBGWarning(format, args...)
#   define DBGInformation(format, args...)
#   define DBGAssert(condition)
#   define DBGParameterAssert(condition)
#else
#   if DEBUG_SILENCENSLOG
#       ifdef NSLog
#           undef NSLog
#       endif
#       define NSLog(format, args...)
#   endif
#   define DBGMessage(format, args...)                      \
        fprintf(stderr, "%s", Format(@"[%s Line " $_quote(__LINE__) "] " format "\n", Basename(@__FILE__), ##args))
#   define DBGError(format, args...) DBGMessage(@"** ERROR **: " format, ##args),assert(!"ERROR")
#   define DBGWarning(format, args...) DBGMessage(@"** WARNING **: " format, ##args)
#   define DBGInformation(format, args...) DBGMessage(@"** INFORMATION **: " format, ##args)
#   define DBGMark(format, args...) ({                      \
        $_pragma(message("Mark Line " $_quote(__LINE__)));  \
        DBGMessage(@"** MARK **: " format, ##args);         \
    })
#   define DBGTrace() ({                                    \
        $_pragma(message("Trace Line " $_quote(__LINE__))); \
        DBGMessage(@"** TRACE **: %@", $methodname);        \
    })
#   define DBGAssert(condition) assert(condition)
#   define DBGParameterAssert(condition) NSParameterAssert(condition)
#endif


#pragma mark - Language Constructs
//*********************************************************************************************************************//

/**
 * This macro is syntactic sugar to allow the use
 * of objects in switch statements. Each case allows
 * multiple matches, and there is no fall-through.
 * The following statement:
 * $_switch(@2) {
 *      $_case(@0, @1, @2):
 *          NSLog(@"match");
 *      $_default:
 *          NSLog(@"no match");
 * }
 * will print match, since @2 is one of the members
 * listed in the first $_case. Note the absence of
 * "break", since there is no fall-through.
 */
#pragma mark [ # ] $switch(<value>) {
#define $switch(value...)                                                               \
    $_declare(id $__switchvalue = value)                                                \
        if (NO)
#   define $__switchcase(condition...)                                                  \
        } else if (condition) {                                                         \
            $_pragma(clang diagnostic push)                                             \
            $_pragma(clang diagnostic ignored "-Wunused-label")                         \
            $_unique($__switchcaselabel_)                                               \
            $_pragma(clang diagnostic pop)

#   pragma mark [ # ] $case(<values...>): <code>
#   define $case(values...) $__switchcase([@Set(values) containsObject:$__switchvalue])

#   pragma mark [ # ] $default: <code>
#   define $default $__switchcase(YES)
#pragma mark [ # ] }

/**
 * This macro defines a "nested function" that can be
 * called recursively. This macro is used in this fashion:
 * <return-type> $sub(<function-name>, <function-params...>)
 * {
 *      <code>
 * };
 * <function-parameters> are space-separated type-name pairs,
 * as in typical function parameter declarations.
 */
#pragma mark [ # ] <return-type> $sub(<name>, <parameters...>) { <code> }
#define $sub(name, parameters...)                           \
    (^name)(parameters);                                    \
    __typeof__(name) $_concat(__sub_, name);                \
    $_do(name,,({name = $_concat(__sub_, name);}))          \
        $_declare(__weak __block __typeof__(name) name)     \
            name = $_concat(__sub_, name) = ^(parameters)

/**
 * This macro allows Python-like unpacking of its arguments. 
 * Expressions that evaluate to NSArrays or NSDictionarys are
 * automatically assigned to given declared variables.
 * $tup(o1, o2) = @[ @1, @2 ] assigns @1 to o1, and @2 to o2. 
 * $tup(o1, o2) = @{@"o2": @2, @"o1": @1} does a similar 
 * operation.
 */
#pragma mark [ # ] $tup(<variables...>)
#define $tup(variables...)                                          \
    $_declare(id tuple)                                             \
    $_do(__COUNTER__,, ({                                           \
        if ([tuple isKindOfClass:[NSArray class]]) {                \
            $_foreach($__tuparray, (variables), tuple);             \
        } else if ([tuple isKindOfClass:[NSDictionary class]]) {    \
            $_foreach($__tupdictionary, (variables), tuple);        \
        } else {                                                    \
            @throw                                                  \
            [NSException                                            \
             exceptionWithName  :NSInternalInconsistencyException   \
             reason             :@Format(@"%@ not a tuple", tuple)  \
             userInfo           :nil];                              \
        }                                                           \
    })) tuple
#   define $__tuparray(index, variable, tuple)                      \
        $_if(variable)                                              \
        (variable = index <[tuple count]? tuple[index]:nil;)()
#   define $__tupdictionary(index, variable, tuple)                 \
        $_if(variable)                                              \
        (variable = tuple [@$_quote(variable)];)()

/**
 * These macros are Python-style loop statements, which
 * allow an else block after the main loop body. The else
 * block is executed if the loop body does not break.
 * An example of a for-loop usage is as follows:
 * $for (int i = 0; i < 6; i ++) {
 *      if (i == 6) {
 *          NSLog(@"This will never print");
 *          break;
 *      }
 * } else {
 *      NSLog(@"Else block executed.");
 * }
 * In the example above, if i is tested for a value of 5 
 * instead of 6, the else block would not be executed.
 */
#pragma mark [ # ] $for(<init-expression>; <test-expression>; <increment-expression>)
#define $for(statements...) $___for(__COUNTER__, statements)
#   define $___for(macroid, statements...)                                                  \
        if (YES) {                                                                          \
            goto $_concat(__loop_, macroid);                                                \
        $_concat(__loop_break_, macroid): ;                                                 \
        } else $_concat(__else_clause_, macroid):                                           \
            $_pragma(clang diagnostic push)                                                 \
            $_pragma(clang diagnostic ignored "-Wdangling-else")                            \
            if (NO)                                                                         \
                while (YES)                                                                 \
                    if (YES) {                                                              \
                        goto $_concat(__else_clause_, macroid);                             \
                    } else $_concat(__loop_, macroid):                                      \
                        for (statements)                                                    \
                            if (YES) {                                                      \
                                goto $_concat(__loop_body_, macroid);                       \
                            $_concat(__loop_continue_, macroid): ;                          \
                            } else                                                          \
                                while (YES)                                                 \
                                    if (YES) {                                              \
                                        goto $_concat(__loop_break_, macroid);              \
                                    } else                                                  \
                                        while (YES)                                         \
                                            if (YES) {                                      \
                                                goto $_concat(__loop_continue_, macroid);   \
                                            } else $_concat(__loop_body_, macroid):         \
            $_pragma(clang diagnostic pop)

#pragma mark [ # ] $while(<condition-expression>)
#define $while(condition...) $for ( ;!! (condition); )

#define $detach                                                                                                     \
    $_declare(void(^block)(void))                                                                                   \
    $_do(__COUNTER__,, ({dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), block);}))   \
    block = ^

#pragma mark - Type Manipulation
//*********************************************************************************************************************//

/**
 * This macro checks for compatibility between two 
 * given types. AreTypesCompatible(int, NSInteger)
 * expands to 1, since NSInteger is a typedef of int.
 */
#ifdef __cplusplus
template <typename type1, typename type2> struct __AreTypesCompatible {
    static const int result = 0;
};

template <typename type1> struct __AreTypesCompatible<type1, type1> {
    static const int result = 1;
};

#   define AreTypesCompatible(type1, type2) __AreTypesCompatible<type1, type2>::result
#else
#   define AreTypesCompatible(type1, type2) __builtin_types_compatible_p(type1, type2)
#endif

/**
 * This macro converts any data type into a pointer.
 * int *pointer = Pointerify(16) is a valid expression.
 */
#ifdef __cplusplus
#   define Pointerify(var) ({__typeof__(var) pointer[] = {var}; pointer;})
#else
#   define Pointerify(var) ((__typeof__(var)[]){var})
#endif

/**
 * These macros bridge between Objc objects
 * and their corresponding void * types.
 */
#define Voidify(object) ((__bridge void *)(object))
#define Objectify(value) ((__bridge id)(value))

/**
 * These macros can wrap arbitrary objects 
 * into an NSData or NSValue object.
 */
#define Datafy(var) [NSData dataWithBytes:Pointerify(var) length:sizeof(var)]
#define Valuefy(var) [NSValue valueWithBytes:Pointerify(var) objCType:@encode(__typeof__(var))]

/**
 * This macro unwraps an NSValue object into a given type.
 */
#define Unwrap(object, type) ({type value = {0}; [object getValue:&value]; value;})

/**
 * This macro wraps a C++ STL vector in an NSData object.
 */
#define WrapSTLVector(vector) [NSData dataWithBytes:&(vector)[0] length:((vector).size() * sizeof((vector)[0]))]


#pragma mark - Expression Attributes
//*********************************************************************************************************************//

/**
 * This macro marks an argument as unused, and 
 * generates a unique name for it. Only the type 
 * of the argument is needed as a parameter.
 */
#define __UNUSEDARG(type) __unused type $_unique(__)

/**
 * These macros act like function attributes for methods.
 * Appending them after method names in implementation
 * files will invoke specific behaviors. __VIRTUALMETHOD__
 * causes a method to do nothing. __ABSTRACTMETHOD__ and
 * __ILLEGALMETHOD__ will cause different exceptions to be 
 * thrown if the marked methods are called.
 */
#define __VIRTUALMETHOD__                               \
    $_pragma(clang diagnostic push)                     \
    $_pragma(clang diagnostic ignored "-Wreturn-type")  \
    {}                                                  \
    $_pragma(clang diagnostic pop)

#if !DEBUG
#   define __ABSTRACTMETHOD__ __VIRTUALMETHOD__
#else
#   define __ABSTRACTMETHOD__ {                                                                     \
        @throw                                                                                      \
        [NSException exceptionWithName  :NSInternalInconsistencyException                           \
                     reason             :@Format(@"abstract %@ must be overridden", $methodname)    \
                     userInfo           :nil];                                                      \
    }
#endif

#if !DEBUG
#   define __ILLEGALMETHOD__ __VIRTUALMETHOD__
#else
#   define __ILLEGALMETHOD__ {                                                                  \
        @throw                                                                                  \
        [NSException exceptionWithName  :NSInternalInconsistencyException                       \
                     reason             :@Format(@"illegal %@ must not be called", $methodname) \
                     userInfo           :nil];                                                  \
    }
#endif


#pragma mark - Class Utilities
//*********************************************************************************************************************//

/**
 * These macros check if a given object is an Objective-C 
 * class object, and if a given type is an Objective-C class.
 */
#define IsClassObject(object) class_isMetaClass(object_getClass(object))
#define IsObjectType(type) AreTypesCompatible(type, id)

/**
 * This is a convenience macro for checking if a given 
 * key path is valid at compile time. If the key
 * path is not valid, an error is generated.
 */
#define Keypath(args...)                                            \
    $_pragma(clang diagnostic push)                                 \
    $_pragma(clang diagnostic ignored "-Warc-repeated-use-of-weak") \
    $_concat($__keypath, $_argc(args))(args)                        \
    $_pragma(clang diagnostic pop)
#   define $__keypath1(path) (((void)(NO && ((void)path, NO)), strrchr($_quote(path), '.') +1))
#   define $__keypath2(object, path) (((void)(NO && ((void)object.path, NO)), $_quote(path)))


#pragma mark - Object Utilities
//*********************************************************************************************************************//

#import "NSArray+Utilities.h"

/**
 * These convenience macros for creating and 
 * manipulating strings. Macros that expand to 
 * strings are prefixed with '@' when called.
 */
#define StringConst(token) NSString *const token = @$_quote(token)

#define Format(format, args...) ([NSString stringWithFormat:format, ##args].UTF8String)
#define Join(string1, string2, strings...) [string1, string2, ##strings].join

#pragma mark [ # ] $split(<string>, <token?>)
#define $split(string, token...) $_concat($__split, $_argc(token))(string, ##token)
#   define $__split0(string) $__split1(string, @" ")
#   define $__split1(string, token) [string componentsSeparatedByString:token]

#define Chomp(string, token...) $_concat($__Chomp, $_argc(token))(string, ##token)
#   define $__Chomp0(string) $__strip1(string, @" ")
#   define $__Chomp1(string, token) (({                         \
    NSString *prefix = token;                                   \
    NSString *result;                                           \
    if (![string hasPrefix:prefix]) {                           \
        result = string;                                        \
    } else {                                                    \
        result = [string substringFromIndex:[prefix length]];   \
    }                                                           \
    result;                                                     \
}).UTF8String)

#define Chop(string, token...) $_concat($__Chop, $_argc(token))(string, ##token)
#   define $__Chop0(string) $__strip1(string, @" ")
#   define $__Chop1(string, token) (({                                      \
    NSString *suffix = token;                                               \
    NSString *result;                                                       \
    if (![string hasSuffix:suffix]) {                                       \
        result = string;                                                    \
    } else {                                                                \
        result = [string substringToIndex:[string length]-[suffix length]]; \
    }                                                                       \
    result;                                                                 \
}).UTF8String)

#define Dirname(path) ([path stringByDeletingLastPathComponent].UTF8String)
#define Basename(path) ([path lastPathComponent].UTF8String)

#define MutateStringIndex(string, index, mutate) ({     \
    char array[string.length+1];                        \
    strcpy(array, [string UTF8String]);                 \
    array[index] = (char)mutate((int)(array[index]));   \
    @(array);                                           \
})

#define UpperCamelCase(string) (MutateStringIndex(string, 0, toupper).UTF8String)
#define LowerCamelCase(string) (MutateStringIndex(string, 0, tolower).UTF8String)

/**
 * These convenience macros for creating and manipulating 
 * collections. $minus(<collection>, <array>) removes elements 
 * in <array> from <collection>. $plus(<collection>, <elements>) 
 * appends <elements> to  <collections>. @Set(<objects...>)
 * creates an NSSet with <objects>.
 */
#define Set(values...) [values].uniqueValues

#pragma mark [ # ] $minus(<collection>, <array>)
#define $minus(collection, array) ({                                            \
    id copy = [collection mutableCopy];                                         \
    if ([copy respondsToSelector:@selector(removeObjectsForKeys:)]) {           \
        [copy removeObjectsForKeys:array];                                      \
    } else if ([copy respondsToSelector:@selector(removeObjectsForKeys:)]) {    \
        [copy removeObjectsInArray:array];                                      \
    }                                                                           \
    [copy copy];                                                                \
})

#pragma mark [ # ] $plus(<collection>, <elements>)
#define $plus(collection, elements) ({                                          \
    id copy = [collection mutableCopy];                                         \
    if ([copy respondsToSelector:@selector(addEntriesFromDictionary:)]) {       \
        [copy addEntriesFromDictionary:(id)elements];                           \
    } else if ([copy respondsToSelector:@selector(addObjectsFromArray:)]) {     \
        [copy addObjectsFromArray:(id)elements];                                \
    }                                                                           \
    [copy copy];                                                                \
})


#pragma mark - Block Utilities
//*********************************************************************************************************************//

/**
 * This inline function gets the Objective-C encoding 
 * of a given block, in a format similar to method encodings.
 */
static inline const char *BLKEncoding(id block) {
    static int kBLKHasSignature = (1 << 30);
    static int kBLKHasCopyDispose = (1 << 25);

    struct BLKDescriptorType {
        unsigned long int reserved;
        unsigned long int size;

        union {
            const char *signature1;

            struct {
                void        (*copyHelper)(void *, void *);
                void        (*disposeHelper)(void *);
                const char  *signature2;
            };
        };
    };

    struct BLKLiteralType {
        void    *isa;
        int     flags;
        int     reserved;
        void    (*invoke)(void *, ...);

        struct BLKDescriptorType *descriptor;
    };

    struct BLKLiteralType *blockRef = (__bridge struct BLKLiteralType *)block;

    if (blockRef->flags & kBLKHasSignature) {
        if (blockRef->flags & kBLKHasCopyDispose) {
            return blockRef->descriptor->signature2;
        }

        return blockRef->descriptor->signature1;
    }

    assert(NO);

    return NULL;
}

/**
 * These macros take care of creating weak references for use 
 * with blocks in order to avoid retain cycles. For example:
 * $weakify(self) {
 *      [self callMethodWithRetainedBlock:^{
 *          $strongify(self) {
 *              [self callAnotherMethod];
 *          }
 *      }];
 * }
 * $weakify will generate an error if the objects passed to it
 * are not passed to a corresponding $strongify(s) or $safify(s)
 * within its scope. $strongify and $safify will generate errors
 * if the objects passed to them are not used within their scopes.
 * $weak, $strong, and $safe behave in exactly the same way except 
 * that they affect their entire current scope.
 */
#pragma mark [ # ] $weakify(<objects...>) {
#define $weakify(objects...)                                                            \
    $_pragma(clang diagnostic push)                                                     \
    $_pragma(clang diagnostic ignored "-Wgcc-compat")                                   \
    $_pragma(clang diagnostic error "-Wunused-variable")                                \
    $_foreach($__weakify, (objects),)                                                   \
    $_pragma(clang diagnostic pop)
#   define $__weakify(_0, object, _2)                                                   \
        for (__weak __typeof__(object) $_concat(__weak_, object) = object;; ({ break; }))

#   pragma mark [ # ] $strongify(<objects...>) { <code> }
#   define $strongify(objects...)                                                               \
        $_pragma(clang diagnostic push)                                                         \
        $_pragma(clang diagnostic ignored "-Wshadow")                                           \
        $_pragma(clang diagnostic ignored "-Wgcc-compat")                                       \
        $_pragma(clang diagnostic error "-Wunused-variable")                                    \
        $_foreach($__strongify, (objects),)                                                     \
        $_pragma(clang diagnostic pop)
#       define $__strongify(_0, object, _2)                                                     \
            for (__strong __typeof__(object) object = $_concat(__weak_, object);; ({ break; }))

#   pragma mark [ # ] $safify(<objects...>) { <code> }
#   define $safify(objects...)                                                              \
        $_pragma(clang diagnostic push)                                                     \
        $_pragma(clang diagnostic ignored "-Wshadow")                                       \
        $_pragma(clang diagnostic ignored "-Wgcc-compat")                                   \
        $_pragma(clang diagnostic error "-Wunused-variable")                                \
        $_foreach($__safify, (objects),)                                                    \
        $_pragma(clang diagnostic pop)
#       define $__safify(_0, object, _2)                                                    \
            for (__weak __typeof__(object) object = $_concat(__weak_, object);; ({ break; }))
#pragma mark [ # ] }

#pragma mark [ # ] $weak(<objects...>)
#define $weak(objects...)                                               \
    $_pragma(clang diagnostic push)                                     \
    $_pragma(clang diagnostic error "-Wunused-variable")                \
    $_foreach($__weak, (objects),)                                      \
    $_pragma(clang diagnostic pop)
#   define $__weak(_0, object, _2)                                      \
        __weak __typeof__(object) $_concat(__weak_, object) = object;

#pragma mark [ # ] $strong(<objects...>)
#define $strong(objects...)                                             \
    $_pragma(clang diagnostic push)                                     \
    $_pragma(clang diagnostic ignored "-Wshadow")                       \
    $_pragma(clang diagnostic error "-Wunused-variable")                \
    $_foreach($__strong, (objects),)
    $_pragma(clang diagnostic pop)
#   define $__strong(_0, object, _2)                                    \
        __strong __typeof__(object) object = $_concat(__weak_, object);

#pragma mark [ # ] $safe(<objects...>)
#define $safe(objects...)                                               \
    $_pragma(clang diagnostic push)                                     \
    $_pragma(clang diagnostic ignored "-Wshadow")                       \
    $_pragma(clang diagnostic error "-Wunused-variable")                \
    $_foreach($__safe, (objects),)
    $_pragma(clang diagnostic pop)
#   define $__safe(_0, object, _2)                                      \
        __weak __typeof__(object) object = $_concat(__weak_, object);

/**
 * This is a convenience macro that automatically injects the 
 * $weak/$strong macros into a specified block, and returns
 * the block. It can be used as follows:
 * <block-type> <block-name> = 
 * Closure((<block-params...>), (<objects...>))({
 *      <code>
 * });
 * <objects> is the comma-separated list of objects to apply
 * the $weak/$strong macros to. Note that Xcode cannot properly
 * locate breakpoints within this macro, so it is recommended
 * to use the $weakify/$strongify macros instead.
 */
#define Closure(args...)                            \
    $_concat($__closure, $_argc(args))(args)
#   define $__closure1(args) $__closure2(args, ())
#   define $__closure2(args, objects)               \
    (({                                             \
        $weak objects                               \
            ^ args {                                \
                $strong objects                     \
                    $__closure
#   define $__closure(args...)                      \
                    args                            \
            };                                      \
    }))


#pragma mark - Device Information
//*********************************************************************************************************************//

#import <UIKit/UIDevice.h>

static inline NSComparisonResult __systemVersionCompare(NSString *versionString) {
    return [[[UIDevice currentDevice] systemVersion] compare:versionString options:NSNumericSearch];
}

#define SYSTEM_VERSION_EQUAL_TO(version) (__systemVersionCompare(@$_quote(version)) == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(version) (__systemVersionCompare(@$_quote(version)) == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(version) (__systemVersionCompare(@$_quote(version)) != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(version) (__systemVersionCompare(@$_quote(version)) == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(version) (__systemVersionCompare(@$_quote(version)) != NSOrderedDescending)


#pragma mark - Arithmetic
//*********************************************************************************************************************//

#define IsEqual(leftValue, rightValue) !islessgreater(leftValue, rightValue)

#define IsZero(value) IsEqual(value, 0.)
#define IsNegative(value) isless(value, 0.)
#define IsPositive(value) isgreater(value, 0.)

#define IsOdd(value) ((value %2)? YES: NO)
#define IsEven(value) !IsOdd(value)

#define IsPot(value) (((value) != 1) && ((value) & ((value) -1))? NO: YES)

#define DivF3(operand1, operand2, operand3) LFMDivf(LFMDivf(operand1, operand2), operand3)


#pragma mark - Logic
//*********************************************************************************************************************//

#define IsBitAsserted(bits, index) (((bits >> index) & 1) != 0)

#define BitAt(index) (1 << index)

#define BitMask(length) (BitAt(length) -1)
#define BitFieldMask(index, length) (BitMask(length) << index)

#define BitFieldRange(bits, index, length) ((bits >> index) & BitMask(length))
#define SetBitFieldRange(bits, value, index, length) \
    (bits = (bits &~ BitFieldMask(index, length)) | ((value & BitMask(length)) << index))


#pragma mark - Object Macros
//*********************************************************************************************************************//

#define $screenscale [[UIScreen mainScreen] scale]
#define $screensize [[UIScreen mainScreen] bounds].size

#define $devicename [[UIDevice currentDevice] name]

#define $buildstring [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]
#define $versionstring [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]

#define $uuid [[NSUUID UUID] UUIDString]

#define $selstring(selector) NSStringFromSelector(@selector(selector))
#define $protostring(protocol_) NSStringFromProtocol(@protocol(protocol_))
#define $class [self class]
#define $classname NSStringFromClass($class)
#define $methodname NSStringFromSelector(_cmd)

#define $notifier [NSNotificationCenter defaultCenter]
#define $filemanager [NSFileManager defaultManager]
#define $userdefaults [NSUserDefaults standardUserDefaults]
#define $mainqueue [NSOperationQueue mainQueue]
#define $currentqueue [NSOperationQueue currentQueue]?:$mainqueue
#define $backgroundqueue [NSOperationQueue new]
#define $mainthread [NSThread mainThread]
#define $currentthread [NSThread currentThread]?:$mainthread
#define $backgroundthread [NSThread new]


/**
 * This is here for quickly testing other macros. Xcode 
 * will generate a message containing the expanded form 
 * if a macro is written between the braces.
 */
#if DEBUG && ( 0 )
#pragma mark [ TESTER ]
$_pragma(message($_quote({
    <#macro#>
})))
#endif



//*********************************************************************************************************************//
#pragma mark -
#pragma mark Deprecated
#pragma mark -
//*********************************************************************************************************************//




#define DEBUG_DISABLE_BITMAP_FONTS 0
#define DEBUG_DISABLE_BACKGROUND_RENDERING 1


#define proxyclass(name, delegate) \
    interface name : LDelegateProxy < delegate > @end @implementation name @end
