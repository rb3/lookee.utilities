//
//  NSAttributedString+Attributes.m
//  Writeability
//
//  Created by Ryan on 1/26/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "NSAttributedString+Attributes.h"





//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSAttributedString+Attributes
#pragma mark -
//*********************************************************************************************************************//





@implementation NSAttributedString (Attributes)

#pragma mark - Public Methods
//*********************************************************************************************************************//

- (UIColor *)colorAtIndex:(NSUInteger)index
{
    return [self
            attribute       :NSForegroundColorAttributeName
            atIndex         :index
            effectiveRange  :NULL];
}

- (UIFont *)fontAtIndex:(NSUInteger)index
{
    return [self
            attribute       :NSFontAttributeName
            atIndex         :index
            effectiveRange  :NULL];
}

- (UIColor *)lastColor
{
    return [self colorAtIndex:self.length-1];
}

- (UIFont *)lastFont
{
    return [self fontAtIndex:self.length-1];
}

@end
