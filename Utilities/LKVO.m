//
//  LKVO.m
//  Writeability
//
//  Created by Ryan on 11/12/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LKVO.h"

#import "LMacros.h"

#import "NSObject+Utilities.h"

#import <objc/message.h>
#import <objc/runtime.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark Private Classes
#pragma mark -
//*********************************************************************************************************************//




@interface LKVOObservation : NSObject <LKVOProtocol>
{
    @package
    /*
     * _observer and _target cannot be declared __weak,
     * otherwise they become nil when the dealloc method is 
     * called. They are still valid pointers at that point, 
     * and must be accessed by the auto-deregister machinery.
     */
    id                          __observer  __unsafe_unretained;
    id                          __target    __unsafe_unretained;
    id                          _observer   __weak;
    id                          _target     __weak;
    NSSet                       *_keyPaths;
    NSKeyValueObservingOptions  _options;
    SEL                         _selector;
    id                          _userInfo;
}

- (instancetype)
initWithObserver:(id                        )observer
ofTarget        :(id                        )target
forKeyPaths     :(NSSet *                   )keyPaths
selector        :(SEL                       )selector
userInfo        :(id                        )userInfo
options         :(NSKeyValueObservingOptions)options;

- (void)_remove;

@end



@interface LKVONotification()

- (instancetype)
initWithKeyPath :(NSString *    )keyPath
change          :(NSDictionary *)change;

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LKVO
#pragma mark -
// Derived from the MAKVONotificationCenter Project @ https://github.com/mikeash/MAKVONotificationCenter
//*********************************************************************************************************************//




@implementation LKVO

#pragma mark - Static Objects
//*********************************************************************************************************************//

static const char * const kLKVOObservationKey = "LKVOObservation";


#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)allObservers
{
    static LKVO             *observer   = nil;
    static dispatch_once_t	predicate   = 0;

    dispatch_once(&predicate, ^ {
        observer = [LKVO new];
    });

    return observer;
}

+ (void)swizzleObjectClass:(id)object
{
    static NSMutableSet *classes;
    {
        static dispatch_once_t token;

        dispatch_once(&token, ^{
            classes = [NSMutableSet new];
        });
    }

    if (object) {
        @synchronized (classes) {
            Class class = [object class];

            if (![classes containsObject:class]) {
                [[object class] overrideDeallocWithBlock:^(__unsafe_unretained id object, void(^dealloc)(void)) {
                    @autoreleasepool {
                        NSSet *observations = [[object associatedValueForKey:kLKVOObservationKey] copy];
                        
                        for (LKVOObservation *observation in observations) {
                            [observation _remove];
                        }
                    }

                    dealloc();
                }];

                [classes addObject:class];
            }
        }
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (id <LKVOProtocol>)
addObserver :(id                                                )observer
ofTarget    :(id                                                )target
forKeyPath  :(id <NSFastEnumeration>                            )keyPath
options     :(NSKeyValueObservingOptions                        )options
block       :(void(^)(__weak id, __weak id, LKVONotification *) )block
{
    return [self
            addObserver :observer
            ofTarget    :target
            forKeyPath  :keyPath
            selector    :NULL
            userInfo    :[block copy]
            options     :options];
}

- (id <LKVOProtocol>)
addObserver :(id                        )observer
ofTarget    :(id                        )target
forKeyPath  :(id <NSFastEnumeration>    )keyPath
selector    :(SEL                       )selector
userInfo    :(id                        )userInfo
options     :(NSKeyValueObservingOptions)options
{
    [$class swizzleObjectClass:observer];
    [$class swizzleObjectClass:target];

    NSMutableSet *keyPaths = [NSMutableSet set];

    for (NSString *path in keyPath) {
        [keyPaths addObject:path];
    }

    return [[LKVOObservation alloc]
            initWithObserver:observer
            ofTarget        :target
            forKeyPaths     :keyPaths
            selector        :selector
            userInfo        :userInfo
            options         :options];
}

- (void)
removeObserver  :(id                    )observer
ofTarget        :(id                    )target
forKeyPath      :(id <NSFastEnumeration>)keyPath
selector        :(SEL                   )selector
{
    @autoreleasepool {
        NSMutableSet *observerObservations  = ([observer associatedValueForKey:kLKVOObservationKey]?: [NSMutableSet new]);
        NSMutableSet *targetObservations    = ([target associatedValueForKey:kLKVOObservationKey]?: [NSMutableSet new]);
        NSMutableSet *allObservations       = [NSMutableSet set];

        NSMutableSet *keyPaths              = [NSMutableSet set];

        for (NSString *path in keyPath) {
            if (![path isKindOfClass:[NSString class]]) {
                DBGError(@"Invalid key path %@", path);
                return;
            }

            [keyPaths addObject:path];
        }

        @synchronized (observerObservations) {
            [allObservations unionSet:observerObservations];
        }

        @synchronized (targetObservations) {
            [allObservations unionSet:targetObservations];
        }

        for (LKVOObservation *observation in allObservations) {
            if ((!observer  || observation->_observer == observer               ) &&
                (!target    || observation->_target == target                   ) &&
                (!keyPath   || [observation->_keyPaths isEqualToSet:keyPaths]   ) &&
                (!selector  || observation->_selector == selector               )) {
                [observation remove];
            }
        }
    }
}

- (void)removeObservation:(id <LKVOProtocol>)observation
{
    [observation remove];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LKVONotification
#pragma mark -
//*********************************************************************************************************************//




@interface LKVONotification()
{
    NSDictionary *_change;
}

@end

@implementation LKVONotification

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)
initWithKeyPath :(NSString *    )keyPath
change          :(NSDictionary *)change
{
    if ((self = [super init])) {
        _keyPath    = keyPath;
        _change     = change;
    }

    return self;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (NSKeyValueChange)kind
{
    return [_change[ NSKeyValueChangeKindKey ] unsignedIntegerValue];
}

- (id)oldValue
{
    return _change[ NSKeyValueChangeOldKey ];
}

- (id)newValue
{
    return _change[ NSKeyValueChangeNewKey ];
}

- (NSIndexSet *)indices
{
    return _change[ NSKeyValueChangeIndexesKey ];
}

- (BOOL)isPrior
{
    return [_change[ NSKeyValueChangeNotificationIsPriorKey ] boolValue];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LKVOObservation
#pragma mark -
//*********************************************************************************************************************//





@implementation LKVOObservation

#pragma mark - Static Objects
//*********************************************************************************************************************//

static void *kLKVOObservationContext = &kLKVOObservationContext;


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)
initWithObserver:(id                        )observer
ofTarget        :(id                        )target
forKeyPaths     :(NSSet *                   )keyPaths
selector        :(SEL                       )selector
userInfo        :(id                        )userInfo
options         :(NSKeyValueObservingOptions)options
{
    DBGParameterAssert((observer   != nil) &&
                        (target     != nil) &&
                        (keyPaths   != nil));

    if ((self = [super init])) {
        _enabled    = YES;
        _observer   = observer;
        __observer  = observer;
        _selector   = selector;
        _userInfo   = userInfo;
        _target     = target;
        __target    = target;
        _keyPaths   = keyPaths;
        _options    = options;

        for (NSString *keyPath in keyPaths) {
            if ([target isKindOfClass:[NSArray class]]) {
                [target
                 addObserver        :self
                 toObjectsAtIndexes :[NSIndexSet indexSetWithIndexesInRange:((NSRange) { 0, [target count] })]
                 forKeyPath         :keyPath
                 options            :options
                 context            :kLKVOObservationContext];
            } else {
                [target
                 addObserver:self
                 forKeyPath :keyPath
                 options    :options
                 context    :kLKVOObservationContext];
            }
        }

        NSMutableSet *observations;

        if (observer) {
            @synchronized (observer) {
                if (!(observations = [observer associatedValueForKey:kLKVOObservationKey])) {
                    observations = [NSMutableSet set];

                    [observer setAssociatedValue:observations forKey:kLKVOObservationKey];
                }
            }

            @synchronized (observations) {
                [observations addObject:self];
            }
        }

        @synchronized (target) {
            if (!(observations = [_target associatedValueForKey:kLKVOObservationKey])) {
                observations = [NSMutableSet set];

                [target setAssociatedValue:observations forKey:kLKVOObservationKey];
            }
        }

        @synchronized (observations) {
            [observations addObject:self];
        }
    }
    
    return self;
}


#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (NSString *)description
{
    return [NSString stringWithFormat:@"<%@: %p, observer: %@ target: %@ keypaths: <%@>>",
            $classname,
            self,
            _observer,
            _target,
            [_keyPaths.allObjects componentsJoinedByString:@","]];
}


#pragma mark - Protected Methods
//*********************************************************************************************************************//

- (void)_remove
{
    @try {
        if ([__target isKindOfClass:[NSArray class]]) {
            for (NSString *keyPath in _keyPaths) {
                [__target
                 removeObserver         :self
                 fromObjectsAtIndexes   :[NSIndexSet indexSetWithIndexesInRange:((NSRange) { 0, [__target count] })]
                 forKeyPath             :keyPath
                 context                :kLKVOObservationContext];
            }
        } else {
            for (NSString *keyPath in _keyPaths) {
                [__target
                 removeObserver :self
                 forKeyPath     :keyPath
                 context        :kLKVOObservationContext];
            }
        }
    }
    @catch (NSException *exception) {
        DBGWarning(@"%@", exception.description);
    }
    @finally {
        NSMutableSet *observations;

        if (__observer) {
            observations = [__observer associatedValueForKey:kLKVOObservationKey];

            @synchronized (observations) {
                [observations removeObject:self];
            }
        }

        observations = [__target associatedValueForKey:kLKVOObservationKey];

        @synchronized (observations) {
            [observations removeObject:self];
        }
        
        __observer  = nil;
        __target    = nil;
        _keyPaths   = nil;
    }
}


#pragma mark - LKVOProtocol
//*********************************************************************************************************************//

@synthesize enabled = _enabled;

- (BOOL)isValid
{
    return (_target != nil) && (_observer != nil);
}

- (void)remove
{
    if ([self isValid]) {
        [self _remove];

        _observer   = nil;
        _target     = nil;
    }
}


#pragma mark - KVO Callbacks
//*********************************************************************************************************************//

- (void)
observeValueForKeyPath  :(NSString *    )keyPath
ofObject                :(__unused id   )__
change                  :(NSDictionary *)change
context                 :(void *        )context
{
    if (context == kLKVOObservationContext) {
        if (_enabled) {
            if (_selector) {
                ((void(*)(id, SEL, NSString *, id, NSDictionary *, id))
                 objc_msgSend)(_observer,
                               _selector,
                               keyPath,
                               _target,
                               change,
                               _userInfo);
            } else {
                LKVONotification *notification = [[LKVONotification alloc]
                                                  initWithKeyPath   :keyPath
                                                  change            :change];

                ((void(^)(__weak id, __weak id, LKVONotification *))_userInfo)(_observer, _target, notification);
            }
        }
    } else {
        [super
         observeValueForKeyPath :keyPath
         ofObject               :_target
         change                 :change
         context                :context];
    }
}


@end
