//
//  NSArray+Utilities.h
//  Writeability
//
//  Created by Ryan on 5/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSArray (Shuffled)

@property (nonatomic, readonly, strong) NSArray         *shuffledCopy;
@property (nonatomic, readonly, strong) NSMutableArray  *shuffledMutableCopy;

@end


@interface NSArray (String)

- (NSString *)join;

@end


@interface NSArray (Set)

- (NSSet *)uniqueValues;

@end
