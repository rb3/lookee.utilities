//
//  LQueuingProxy.h
//  Writeability
//
//  Created by Ryan on 7/29/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LProxy.h"


@interface LQueuingProxy : LProxy

@property (nonatomic, readonly, strong) id $desynchronize;
@property (nonatomic, readonly, strong) id $synchronize;

@end


@interface NSObject (LQueuingProxy)

- (instancetype)$desynchronize;
- (instancetype)$synchronize;

@end
