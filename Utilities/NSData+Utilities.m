//
//  NSData+Utilities.m
//  Writeability
//
//  Created by Ryan Blonna on 14/9/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "NSData+Utilities.h"

#import "LMacros.h"

#import "LBase.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSData+Representation
#pragma mark -
//*********************************************************************************************************************//




@implementation NSData (Representation)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype)dataFromBase58Representation:(NSString *)representation
{
    return [[self alloc] initWithBase58Representation:representation];
}

+ (instancetype)dataFromBase64Representation:(NSString *)representation
{
    return [[self alloc] initWithBase64Representation:representation];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithBase58Representation:(NSString *)representation
{
    unsigned long   length      = 0;
    unsigned char   *characters = LBase58Decode(representation.UTF8String, representation.length, &length);

    if (!characters) {
        DBGError(@"Invalid base 58 string %@", representation);
        return nil;
    }

    if ((self = [self initWithBytesNoCopy:characters length:length])) {

    }

    return self;
}

- (instancetype)initWithBase64Representation:(NSString *)representation
{
    if (SYSTEM_VERSION_LESS_THAN(7.0)) {
        self = [self initWithBase64Encoding:representation];
    } else {
        self = [self initWithBase64EncodedString:representation options:0];
    }

    if (self) {

    }

    return self;
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (NSString *)base58Representation
{
    char *characters = LBase58Encode(self.bytes, self.length, NULL);

    NSString *encoded = nil;

    if (characters) {
        encoded = @(characters);

        free(characters);
    }

    return encoded;
}

- (NSString *)base64Representation
{
    if (SYSTEM_VERSION_LESS_THAN(7.0)) {
        return [self base64Encoding];
    }

    return [self base64EncodedStringWithOptions:0];
}

@end




//*********************************************************************************************************************//
#pragma mark -
#pragma mark NSData+NSValue
#pragma mark -
//*********************************************************************************************************************//




@implementation NSData (NSValue)

#pragma mark - Class Methods
//*********************************************************************************************************************//

+ (instancetype) dataWithValue:(NSValue *)value
{
    return [self.alloc initWithValue: value];
}

+ (instancetype)dataWithCGAffineTransform:(CGAffineTransform)transform
{
    return [[self alloc] initWithCGAffineTransform:transform];
}

+ (instancetype)dataWithCGPoint:(CGPoint)point
{
    return [[self alloc] initWithCGPoint:point];
}

+ (instancetype)dataWithCGRect:(CGRect)rect
{
    return [self.alloc initWithCGRect:rect];
}

+ (instancetype)dataWithCGSize:(CGSize)size
{
    return [[self alloc] initWithCGSize:size];
}

+ (instancetype)dataWithUIEdgeInsets:(UIEdgeInsets)insets
{
    return [[self alloc] initWithUIEdgeInsets:insets];
}

+ (instancetype)dataWithUIOffset:(UIOffset)offset
{
    return [[self alloc] initWithUIOffset:offset];
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)initWithValue:(NSValue *)value
{
    NSUInteger size = 0;

    NSGetSizeAndAlignment(value.objCType, &size, NULL);

    void *valueData = malloc(size);

    [value getValue:valueData];

    return (self = [self initWithBytesNoCopy:valueData length:size freeWhenDone:YES]);
}

- (instancetype)initWithCGAffineTransform:(CGAffineTransform)transform
{
    return (self = [self initWithBytes:&transform length:sizeof(CGAffineTransform)]);
}

- (instancetype)initWithCGPoint:(CGPoint)point
{
    return (self = [self initWithBytes:&point length:sizeof(CGPoint)]);
}

- (instancetype)initWithCGRect:(CGRect)rect
{
    return (self = [self initWithBytes:&rect length:sizeof(CGRect)]);
}

- (instancetype)initWithCGSize:(CGSize)size
{
    return (self = [self initWithBytes:&size length:sizeof(CGSize)]);
}

- (instancetype)initWithUIEdgeInsets:(UIEdgeInsets)insets
{
    return (self = [self initWithBytes:&insets length:sizeof(UIEdgeInsets)]);
}

- (instancetype)initWithUIOffset:(UIOffset)offset
{
    return (self = [self initWithBytes:&offset length:sizeof(UIOffset)]);
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (CGAffineTransform)CGAffineTransformValue
{
    return *(CGAffineTransform *)(self.bytes);
}

- (CGPoint)CGPointValue
{
    return *(CGPoint *)(self.bytes);
}

- (CGRect)CGRectValue
{
    return *(CGRect *)(self.bytes);
}

- (CGSize)CGSizeValue
{
    return *(CGSize *)(self.bytes);
}

- (UIEdgeInsets)UIEdgeInsetsValue
{
    return *(UIEdgeInsets *)(self.bytes);
}

- (UIOffset)UIOffsetValue
{
    return *(UIOffset *)(self.bytes);
}

@end

