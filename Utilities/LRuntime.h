//
//  LRuntime.h
//  Writeability
//
//  Created by Ryan on 3/22/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//



#import <objc/runtime.h>
#import <string.h>

#import "LWrapper.h"


#ifdef __cplusplus
extern "C" {
#endif



//*********************************************************************************************************************//
#pragma mark -
#pragma mark LRuntime
#pragma mark -
//*********************************************************************************************************************//



#pragma mark - Public Methods
//*********************************************************************************************************************//

#define LRuntimeGetAssociatedValue(class, name, type) ({                            \
    id object = objc_getAssociatedObject(self, $##class##$##name##$##Synthesize);   \
    __builtin_choose_expr                                                           \
    (IsObjectType(type),                                                            \
     [object reference],                                                            \
     Unwrap(object, type));                                                         \
})

#define LRuntimeSetAssociatedValue(class, name, type, value, policy)\
    objc_setAssociatedObject                                        \
    (self, $##class##$##name##$##Synthesize,                        \
     __builtin_choose_expr                                          \
     (!IsObjectType(type),                                          \
      Valuefy(value),                                               \
      policy == kLRunTimeAssociationAssign?                         \
      [LWrapper wrapperWithUnretainedRef:&value]:                   \
      policy == kLRunTimeAssociationWeak?                           \
      [LWrapper wrapperWithWeakRef:&value]:                         \
      [LWrapper wrapperWithStrongRef:&value]),                      \
     __builtin_choose_expr                                          \
     (!IsObjectType(type),                                          \
      kLRunTimeAssociationRetainNonatomic,                          \
      policy == kLRunTimeAssociationWeak?                           \
      kLRunTimeAssociationRetainNonatomic:                          \
      policy == kLRunTimeAssociationAssign?                         \
      kLRunTimeAssociationRetainNonatomic:                          \
      policy))

#define implement(class, name, type, attr...)                                                           \
    dynamic name;                                                                                       \
    __attribute__((constructor))                                                                        \
    static void $##class##$##name##$##Synthesize(void) {                                                \
        Class   this    = objc_getClass($_quote(class));                                                \
        SEL     getter  = NULL;                                                                         \
        SEL     setter  = NULL;                                                                         \
        LRunTimeAssociationPolicy   policy          = LRunTimeAssociationPolicyForProperty(#name,       \
                                                                                           #attr,       \
                                                                                           &getter,     \
                                                                                           &setter);    \
        const char                  *encoding       = @encode(type);                                    \
        if (getter) {                                                                                   \
            char *getterEncoding = NULL; asprintf(&getterEncoding, "%s@:", encoding);                   \
            DBGAssert(getterEncoding != NULL);                                                          \
            IMP getterImplementation = imp_implementationWithBlock(                                     \
                ^(id self) {                                                                            \
                    return LRuntimeGetAssociatedValue(class, name, type);                               \
                }                                                                                       \
            );                                                                                          \
            class_addMethod(this, getter, getterImplementation, getterEncoding);                        \
            free(getterEncoding);                                                                       \
        }                                                                                               \
        if (setter) {                                                                                   \
            char *setterEncoding = NULL; asprintf(&setterEncoding, "v@:%s", encoding);                  \
            DBGAssert(setterEncoding != NULL);                                                          \
            IMP setterImplementation = imp_implementationWithBlock(                                     \
                ^(id self, type value) {                                                                \
                    [self willChangeValueForKey:@$_quote(name)];                                        \
                    LRuntimeSetAssociatedValue(class, name, type, value, policy);                       \
                    [self didChangeValueForKey:@$_quote(name)];                                         \
                }                                                                                       \
            );                                                                                          \
            class_addMethod(this, setter, setterImplementation, setterEncoding);                        \
            free(setterEncoding);                                                                       \
        }                                                                                               \
    }


typedef NS_ENUM(NSInteger, LRunTimeAssociationPolicy) {
    kLRunTimeAssociationAssign          = OBJC_ASSOCIATION_ASSIGN,
    kLRunTimeAssociationRetainNonatomic = OBJC_ASSOCIATION_RETAIN_NONATOMIC,
    kLRunTimeAssociationCopyNonatomic   = OBJC_ASSOCIATION_COPY_NONATOMIC,
    kLRunTimeAssociationRetain          = OBJC_ASSOCIATION_RETAIN,
    kLRunTimeAssociationCopy            = OBJC_ASSOCIATION_COPY,
    
    kLRunTimeAssociationWeak
};

static inline const char *RuntimeTypeWithoutQualifiers(const char *encodedType) {
    while(strchr("rnNoORV", encodedType[0]) != NULL) {
        encodedType += 1;
    }

    return encodedType;
}

static inline LRunTimeAssociationPolicy LRunTimeAssociationPolicyForProperty
(
 const char *propertyName,
 const char *attributeString,
 SEL        *getter,
 SEL        *setter
 ) {
    char *tokenString = strdup(attributeString);

    assert(tokenString != NULL);

    typedef NS_OPTIONS(Byte, PropertyAttribute) {
        kAttributeNone      = 0 << 0,
        kAttributeNonatomic = 1 << 0,
        kAttributeReadonly  = 1 << 1,
        kAttributeWeak      = 1 << 2,
        kAttributeStrong    = 1 << 3,
        kAttributeCopy      = 1 << 4,
        kAttributeAssign    = 1 << 5,
        kAttributeRetain    = 1 << 6
    };

    PropertyAttribute           attributes  = kAttributeNone;
    LRunTimeAssociationPolicy   policy      = kLRunTimeAssociationAssign;

    char *iterator = strtok(tokenString, ",= ");

    while (iterator != NULL) {
        if (strcmp(iterator, "nonatomic") == 0) {
            attributes |= kAttributeNonatomic;
        } else if (strcmp(iterator, "readonly") == 0) {
            attributes |= kAttributeReadonly;
        } else if (strcmp(iterator, "weak") == 0) {
            attributes |= kAttributeWeak;
        } else if (strcmp(iterator, "strong") == 0) {
            attributes |= kAttributeStrong;
        } else if (strcmp(iterator, "copy") == 0) {
            attributes |= kAttributeCopy;
        } else if (strcmp(iterator, "assign") == 0) {
            attributes |= kAttributeAssign;
        } else if (strcmp(iterator, "retain") == 0) {
            attributes |= kAttributeRetain;
        } else if (strncasecmp(iterator, "getter", 6) == 0) {
            iterator    = strtok(NULL, ",= ");
            *getter     = sel_registerName(iterator);
        } else if (strncmp(iterator, "setter", 6) == 0) {
            iterator    = strtok(NULL, ",= ");
            *setter     = sel_registerName(iterator);
        }

        iterator = strtok(NULL, ",= ");
    }

    free(tokenString);

    if ((attributes & kAttributeStrong) || (attributes & kAttributeRetain)) {
        policy = (attributes & kAttributeNonatomic? kLRunTimeAssociationRetainNonatomic: kLRunTimeAssociationRetain);
    } else if (attributes & kAttributeCopy) {
        policy = (attributes & kAttributeNonatomic? kLRunTimeAssociationCopyNonatomic: kLRunTimeAssociationCopy);
    } else if (attributes & kAttributeWeak) {
        policy = kLRunTimeAssociationWeak;
    }

    if (getter && !*getter) {
        *getter = sel_registerName(propertyName);
    }

    if (!(attributes & kAttributeReadonly)) {
        if (setter && !*setter) {
            int     length      = strlen(propertyName)+5;
            char    *selector   = (char *)malloc(length);

            snprintf(selector, length, "set%s:", propertyName);
            
            selector[3]         = toupper(selector[3]);
            *setter             = sel_registerName(selector);
            free(selector);
        }
    }

    return policy;
}


#ifdef __cplusplus
}
#endif
