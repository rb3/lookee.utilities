//
//  LCallbackProxy.h
//  Writeability
//
//  Created by Ryan on 7/31/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LProxy.h"


@interface LCallbackProxy : LProxy

@property (atomic, readwrite, copy, setter = $setCallback:) void(^$callback)(NSInvocation *);


+ (id)proxyWithType:(Class)type;

@end


@interface NSObject (LCallbackProxy)

@property (atomic, readwrite, copy, setter = $setCallback:) void(^$callback)(NSInvocation *);

@end