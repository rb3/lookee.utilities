//
//  LOperationStack.m
//  Writeability
//
//  Created by Ryan on 11/12/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import "LOperationStack.h"





//*********************************************************************************************************************//
#pragma mark -
#pragma mark LOperationStack
#pragma mark -
// Derived from the CBOperationStack Project @ https://github.com/cbrauchli/CBOperationStack
//*********************************************************************************************************************//





@implementation LOperationStack

#pragma mark - Instance Variables
//*********************************************************************************************************************//
{
@private
    NSCondition *_workAvailable;
    NSCondition *_suspendedCondition;
    NSCondition *_allWorkDone;

    NSArray         *_queues;
    NSMutableArray  *_threads;
}


#pragma mark - Static Objects
//*********************************************************************************************************************//

static const    NSUInteger LOperationStackPriorityCount = 5;

static inline   NSUInteger LOperationStackPriority(NSOperation *operation) {
    NSUInteger priority;

    switch ([operation queuePriority]) {
        case NSOperationQueuePriorityVeryLow: {
            priority = 4;
        } break;
        case NSOperationQueuePriorityLow: {
            priority = 3;
        } break;
        case NSOperationQueuePriorityNormal: {
            priority = 2;
        } break;
        case NSOperationQueuePriorityHigh: {
            priority = 1;
        } break;
        case NSOperationQueuePriorityVeryHigh: {
            priority = 0;
        } break;
        default: {
            priority = 2;
        } break;
    }

    return priority;
}


#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

- (instancetype)init
{
    if ((self = [super init])) {
        _workAvailable      = [NSCondition new];
        _suspendedCondition = [NSCondition new];
        _allWorkDone        = [NSCondition new];

        _maxConcurrentOperationCount    = 1;
        _suspended                      = NO;

        NSMutableArray *queues = [NSMutableArray arrayWithCapacity:LOperationStackPriorityCount];

        _queues     = queues;
        _threads    = [NSMutableArray new];

        for (NSUInteger index = 0; index < LOperationStackPriorityCount; ++ index) {
            [queues addObject:[NSMutableArray new]];
        }
        
        NSThread *thread = [[NSThread alloc]
                            initWithTarget  :self
                            selector        :@selector(execute)
                            object          :nil];

        [thread setName:@"com.LOperationStack.thread-0"];

        [_threads addObject:thread];

        [thread start];
    }
  
	return self;
}

- (void)dealloc
{
    [self stop];
}


#pragma mark - Properties
//*********************************************************************************************************************//

@dynamic operations;

- (NSArray *)operations
{
    NSMutableArray *operations = [NSMutableArray arrayWithCapacity:self.operationCount];

    @synchronized(self) {
        for (NSMutableArray *queue in _queues) {
            [operations addObjectsFromArray:queue];
        }
    }

    return operations;
}

@dynamic operationCount;

- (NSUInteger)operationCount
{
    NSUInteger count = 0;

    @synchronized(self) {
        for (NSMutableArray *queue in _queues) {
            count += queue.count;
        }
    }

    return count;
}

@synthesize maxConcurrentOperationCount = _maxConcurrentOperationCount;

- (void)setMaxConcurrentOperationCount:(NSInteger)count
{
    if (count < 0 || count == NSOperationQueueDefaultMaxConcurrentOperationCount) {
        _maxConcurrentOperationCount = 1;
    } else {
        _maxConcurrentOperationCount = count;
    }

    @synchronized(self) {
        NSInteger difference = (_maxConcurrentOperationCount - [_threads count]);

        while (difference > 0) {
            NSThread *thread = [[NSThread alloc]
                                initWithTarget  :self
                                selector        :@selector(execute)
                                object          :nil];

            [thread setName:[NSString stringWithFormat:@"com.LOperationStack.thread-%d", [_threads count]]];

            [_threads addObject:thread];

            [thread start];

            -- difference;
        }

        while (difference < 0) {
            NSThread *thread = [_threads lastObject];
            [_threads removeLastObject];

            [thread cancel];

            ++ difference;
        }
    }

    [_workAvailable lock];
    [_workAvailable broadcast];
    [_workAvailable unlock];
}

@synthesize suspended = _suspended;

- (BOOL)isSuspended
{
    [_suspendedCondition lock];
    BOOL result = _suspended;
    [_suspendedCondition unlock];

    return result;
}

- (void)setSuspended:(BOOL)suspended
{
    if (suspended) {
        [self suspend];
    } else {
        [self resume];
    }
}


#pragma mark - Public Methods
//*********************************************************************************************************************//

- (void)addOperation:(NSOperation *)operation
{
    NSUInteger priority = LOperationStackPriority(operation);

    [_workAvailable lock];

    @synchronized(self) {
        [[_queues objectAtIndex:priority] addObject:operation];
    }

    [_workAvailable signal];
    [_workAvailable unlock];
}

- (void)addOperationWithBlock:(void(^)(void))block
{
    [self addOperation:[NSBlockOperation blockOperationWithBlock:block]];
}

- (void)addOperations:(NSArray *)operations waitUntilFinished:(BOOL)wait
{
    for (NSOperation *operation in operations) {
        [self addOperation:operation];
    }

    if (wait) {
        [self waitUntilAllOperationsAreFinished];
    }
}

- (void)addOperationAtBottomOfStack:(NSOperation *)operation
{
    NSUInteger priority = LOperationStackPriority(operation);

    [_workAvailable lock];

    @synchronized(self) {
        [[_queues objectAtIndex:priority] insertObject:operation atIndex:0];
    }

    [_workAvailable signal];
    [_workAvailable unlock];
}

- (void)cancelAllOperations
{
    [self.operations makeObjectsPerformSelector:@selector(cancel)];
}

- (void)waitUntilAllOperationsAreFinished
{
    BOOL isWorking;

    [_workAvailable lock];
    isWorking = [self hasJob];
    [_workAvailable unlock];

    if (isWorking) {
        [_allWorkDone lock];
        [_allWorkDone wait];
        [_allWorkDone unlock];
    }
}


#pragma mark - Private Methods
//*********************************************************************************************************************//

- (void)resume
{
    [_suspendedCondition lock];

	if ([self isSuspended]) {
        (_suspended = NO);
		[_suspendedCondition broadcast];
    }

	[_suspendedCondition unlock];
}

- (void)suspend
{
	[_suspendedCondition lock];
    (_suspended = YES);
	[_suspendedCondition unlock];
}


- (void)stop
{
    for (NSThread *thread in _threads) {
        [thread cancel];
    }

	[self resume];

    [_workAvailable lock];
	[_workAvailable broadcast];
    [_workAvailable unlock];
}

- (BOOL)hasJob
{
    for (NSMutableArray *queue in _queues) {
        if ([queue count] > 0) {
            return YES;
        }
    }
  
	return NO;
}

- (void)execute
{
    @autoreleasepool {
        NSThread *thread = [NSThread currentThread];
    
        BOOL didRun = NO;

        while(![thread isCancelled]) {
            [_suspendedCondition lock];

            while (_suspended) {
                [_suspendedCondition wait];
            }

            [_suspendedCondition unlock];
      
            if(!didRun) {
                [_workAvailable lock];
        
                if(![self hasJob]){
                    [_allWorkDone lock];
                    [_allWorkDone broadcast];
                    [_allWorkDone unlock];
                }
        
                while (![self hasJob] && ![thread isCancelled]) {
                    [_workAvailable wait];
                }
        
                [_workAvailable unlock];
            }
      
            if (![thread isCancelled]) {
                NSOperation *operation = nil;

                @synchronized(self) {
                    for (NSMutableArray *queue in _queues) {
                        operation = [queue lastObject];

                        if (operation && [operation isReady]) {
                            [queue removeLastObject];
                            break;
                        } else {
                            operation = nil;
                        }
                    }
                }

                if (operation) {
                    [operation start];
                    didRun = YES;
                } else {
                    didRun = NO;
                }
            }
        }
    }
}

@end
