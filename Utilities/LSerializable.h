//
//  LSerializable.h
//  Utilities
//
//  Created by Ryan Blonna on 16/1/15.
//  Copyright (c) 2015 Lookee. All rights reserved.
//


#import <Foundation/Foundation.h>


@protocol LSerializable <NSObject>

@end
