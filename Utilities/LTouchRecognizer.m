//
//  LGestureRecognizer.m
//  Writeability
//
//  Created by Jelo Agnasin on 10/8/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//

#import "LTouchRecognizer.h"

#import <UIKit/UIGestureRecognizerSubclass.h>




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LTouchRecognizer
#pragma mark -
//*********************************************************************************************************************//




@implementation LTouchRecognizer

#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
    return NO;
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer
{
    return NO;
}

- (void)reset
{
    _startPoint     = CGPointZero;
    _previousPoint  = CGPointZero;
    
    _didBegin       = NO;
    _justBegan      = NO;
    _didMove        = NO;
    
    self.state = UIGestureRecognizerStatePossible;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([event.allTouches count] > 1) {
        self.state = UIGestureRecognizerStateFailed;

        return;
    }

    self.state = UIGestureRecognizerStateBegan;
    
    UITouch *touch  = [event.allTouches anyObject];

    _location       = [touch locationInView:self.view];
    _startPoint     = _location;
    _previousPoint  = _startPoint;

    _didBegin       = YES;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.state == UIGestureRecognizerStateFailed ||
        self.state == UIGestureRecognizerStateCancelled) {
        return;
    }
    
    if ([event.allTouches count] > 1) {
        self.state = UIGestureRecognizerStateCancelled;

        return;
    }

    self.state = UIGestureRecognizerStateChanged;
    
    UITouch *touch  = [event.allTouches anyObject];

    _location       = [touch locationInView:self.view];

    _previousPoint  = _location;
    
    _justBegan      = ![self didMove];
    _didMove        = YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.state == UIGestureRecognizerStateFailed ||
        self.state == UIGestureRecognizerStateCancelled) {
        return;
    }

    if ([event.allTouches count] > 1) {
        self.state = UIGestureRecognizerStateCancelled;
    } else {
        self.state = UIGestureRecognizerStateEnded;
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.state == UIGestureRecognizerStateFailed ||
        self.state == UIGestureRecognizerStateCancelled) {
        return;
    }
    
    self.state = UIGestureRecognizerStateCancelled;
}


@end
