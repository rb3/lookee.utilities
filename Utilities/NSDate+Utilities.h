//
//  NSDate+Utilities.h
//  Utilities
//
//  Created by Ryan Blonna on 20/1/15.
//  Copyright (c) 2015 Lookee. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSDate (TimeStamp)

+ (instancetype)dateFromTimeStamp:(NSString *)timeStamp;


- (instancetype)initWithTimeStamp:(NSString *)timeStamp;


- (NSString *)timeStamp;

@end
