//
//  LTimer.h
//  Writeability
//
//  Created by Ryan on 11/14/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface LTimer : NSObject

@property (nonatomic, readonly, assign) BOOL isRunning;


+ (LTimer *)
timerWithTimeInterval   :(NSTimeInterval)seconds
target                  :(id            )target
selector                :(SEL           )selector
userInfo                :(id            )userInfo;

+ (LTimer *)
timerWithTimeInterval   :(NSTimeInterval)seconds
block                   :(void(^)(void) )block;

- (instancetype)
initWithInterval:(NSTimeInterval)interval
target          :(id            )target
selector        :(SEL           )selector
userInfo        :(id            )userInfo
repeats         :(BOOL          )repeats;

- (instancetype)
initWithInterval:(NSTimeInterval)interval
block           :(void(^)(void) )block
repeats         :(BOOL          )repeats;


- (void)start;
- (void)startInRunLoop:(NSRunLoop *)runLoop;
- (void)startInRunLoop:(NSRunLoop *)runLoop withMode:(NSString *)mode;

- (void)expire;
- (void)stop;

@end
