//
//  UIScrollView+Scroll.h
//  Writeability
//
//  Created by Ryan on 12/19/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <UIKit/UIKit.h>


@interface UIScrollView (Scroll)

- (void)scrollToTop;
- (void)scrollToTopAnimated:(BOOL)animated;

@end
