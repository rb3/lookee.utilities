//
//  LProxy.h
//  Writeability
//
//  Created by Ryan on 12/10/13.
//  Copyright (c) 2013 CNL. All rights reserved.
//


#import <Foundation/Foundation.h>



@protocol LProxyDelegate;



@interface LDelegateProxy : NSObject

- (instancetype)initWithDelegate:(id <LProxyDelegate>)delegate;

@end


@protocol LProxyDelegate <NSObject>
@required

- (id)receiver;

@end
