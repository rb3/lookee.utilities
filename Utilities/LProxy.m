//
//  LProxy.m
//  Writeability
//
//  Created by Ryan on 8/3/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//


#import "LProxy.h"

#import "LMacros.h"




//*********************************************************************************************************************//
#pragma mark -
#pragma mark LProxy
#pragma mark -
//*********************************************************************************************************************//




@implementation LProxy

#pragma mark - Constructors/Destructors
//*********************************************************************************************************************//

+ (id)alloc __ILLEGALMETHOD__;

+ (id)proxy
{
    LProxy *instance = [super alloc];
    {
        instance->objectLock = [NSObject new];
    }

    return instance;
}

+ (id)proxyWithObject:(id)object
{
    DBGParameterAssert(object != nil);

    LProxy *instance = [self proxy];
    {
        instance->object = object;
    }

    return instance;
}


#pragma mark - Properties
//*********************************************************************************************************************//

- (id)$direct
{
    @synchronized(objectLock) {
        return object;
    }
}



#pragma mark - Overridden Methods
//*********************************************************************************************************************//

- (Class)class
{
    @synchronized(objectLock) {
        if (!object) {
            DBGError(@"Method '%@' must be overridden in a subclass", $methodname);
            return nil;
        }

        return [object class];
    }
}

- (NSString *)description
{
    @synchronized(objectLock) {
        return @Join([object description], @" (proxy)");
    }
}

- (NSString *)debugDescription
{
    return [self description];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)selector
{
    @synchronized(objectLock) {
        return [object methodSignatureForSelector:selector];
    }
}

- (id)forwardingTargetForSelector:(SEL)selector
{
    @synchronized(objectLock) {
        return object;
    }
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    @synchronized(objectLock) {
        [invocation setTarget:object];
    }
}

@end
