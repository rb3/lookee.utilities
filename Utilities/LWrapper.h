//
//  LWrapper.h
//  Writeability
//
//  Created by Ryan on 4/10/14.
//  Copyright (c) 2014 CNL. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface LWrapper : NSObject

+ (id)wrapperWithWeakRef:(const void *)pointer;
+ (id)wrapperWithUnretainedRef:(const void *)pointer;
+ (id)wrapperWithStrongRef:(const void *)pointer;

- (id)reference;

@end